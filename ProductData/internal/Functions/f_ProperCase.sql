﻿CREATE FUNCTION [internal].[f_ProperCase] 
( 
    @InputString VARCHAR(4000) 
) 
RETURNS VARCHAR(4000) 
AS 
BEGIN 
-- ==================================================================================================================
-- Author:		Ridgian
-- Create date: 04/03/2014
-- Description:	Function to return a string formatted as each word in lower case with an initial capital letter
-- ------------------------------------------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------------------------------------------
-- Debug Code:
/*
	DECLARE @InString VARCHAR(100) = 'FBI ld THE qUICK brown fOx jUmPeD OVER tHE LaZy DoGs bACK 
	DECLARE @OutString VARCHAR(100)
	SET @OutString = [internal].[InitCap](@InString) 
	PRINT  @InString
	PRINT  @OutString
*/
-- ==================================================================================================================

DECLARE @Index          INT
DECLARE @Char           CHAR(1)
DECLARE @PrevChar       CHAR(1)
DECLARE @OutputString   VARCHAR(255)

SET @OutputString = LOWER(@InputString)
SET @Index = 1

WHILE @Index <= LEN(@InputString)
BEGIN
    SET @Char     = SUBSTRING(@InputString, @Index, 1)
    SET @PrevChar = CASE WHEN @Index = 1 THEN ' '
                         ELSE SUBSTRING(@InputString, @Index - 1, 1)
                    END

    IF @PrevChar IN (' ', ';', ':', '!', '?', ',', '.', '_', '-', '/', '&', '''', '(')
    BEGIN
        IF @PrevChar != '''' OR UPPER(@Char) != 'S'
            SET @OutputString = STUFF(@OutputString, @Index, 1, UPPER(@Char))
    END

    SET @Index = @Index + 1
END

-- Use the f_CapitaliseAbbreviation function to recapitalise any abbreviations
-- SET @OutputString = internal.f_CapitaliseAbbreviation(@OutputString,'FBI')

RETURN @OutputString
END