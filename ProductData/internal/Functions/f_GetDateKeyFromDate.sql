﻿
CREATE FUNCTION [internal].[f_GetDateKeyFromDate] 
(
	@date DATETIME
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @DateKey INT
	
	-- Compute the time key
	SELECT @DateKey = CAST(CONVERT(CHAR(8),@date,112) AS INT)

	-- Do not allow dates before 2000.
	-- Hard-coded for performance - but could be sourced from control table?
	IF @DateKey < 20000101 SET @DateKey = 20000101

	-- Return the result of the function
	RETURN @DateKey

END