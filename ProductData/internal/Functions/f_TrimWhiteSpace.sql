﻿CREATE FUNCTION [internal].[f_TrimWhiteSpace] 
(
	@StringToClean as varchar(8000)
)
RETURNS varchar(8000)
AS
-- ==================================================================================================================
-- Author:	Richard Tennant 
--			(
--			From a function by Israel Cris Valenzuela:
--			http://www.codeproject.com/Tips/330787/LTRIM-RTRIM-doesn-t-always-work
--			)
-- Create date: 28/06/2012
-- Description:	function to remove ALL whitespaces, including non printed (CR, LF, tab, etc), from a string.
-- ------------------------------------------------------------------------------------------------------------------
-- Revision History:
-- 
-- ------------------------------------------------------------------------------------------------------------------
-- ==================================================================================================================
BEGIN	
	--Replace all non printing whitespace characers with Characer 32 whitespace
	--NULL
	Set @StringToClean = Replace(@StringToClean,CHAR(0),CHAR(32));
	--Horizontal Tab
	Set @StringToClean = Replace(@StringToClean,CHAR(9),CHAR(32));
	--Line Feed
	Set @StringToClean = Replace(@StringToClean,CHAR(10),CHAR(32));
	--Vertical Tab
	Set @StringToClean = Replace(@StringToClean,CHAR(11),CHAR(32));
	--Form Feed
	Set @StringToClean = Replace(@StringToClean,CHAR(12),CHAR(32));
	--Carriage Return
	Set @StringToClean = Replace(@StringToClean,CHAR(13),CHAR(32));
	--Column Break
	Set @StringToClean = Replace(@StringToClean,CHAR(14),CHAR(32));
	--Non-breaking space
	Set @StringToClean = Replace(@StringToClean,CHAR(160),CHAR(32));
 
	Set @StringToClean = LTRIM(RTRIM(@StringToClean));
	Return @StringToClean
END