﻿CREATE TABLE [internal].[FileLog] (
    [FileLogKey]       BIGINT          IDENTITY (1, 1) NOT NULL,
    [JobKey]           BIGINT          DEFAULT ((-1)) NOT NULL,
    [FilePath]         NVARCHAR (1000) NOT NULL,
    [FileName]         NVARCHAR (1000) NOT NULL,
    [OriginalFileName] NVARCHAR (1000) NULL,
    [FileLoadDate]     DATETIME        DEFAULT (getdate()) NOT NULL,
    [FIleTypeKey]      INT             CONSTRAINT [DF_FileLog_FileType] DEFAULT ((-1)) NOT NULL,
    [FileStatusKey]    INT             CONSTRAINT [DF_FileLog_FileStatus] DEFAULT ((-1)) NOT NULL,
    [EmailStatusKey]   INT             CONSTRAINT [DF_FileLog_EmailStatus] DEFAULT ((-1)) NOT NULL,
    CONSTRAINT [PK_FileLog] PRIMARY KEY CLUSTERED ([FileLogKey] ASC),
    CONSTRAINT [FK_FileLog_EmailStatus] FOREIGN KEY ([EmailStatusKey]) REFERENCES [internal].[EmailStatus] ([EmailStatusKey]),
    CONSTRAINT [FK_FileLog_FileStatus] FOREIGN KEY ([FileStatusKey]) REFERENCES [internal].[FileStatus] ([FileStatusKey]),
    CONSTRAINT [FK_FileLog_FileType] FOREIGN KEY ([FIleTypeKey]) REFERENCES [internal].[FileType] ([FileTypeKey]),
    CONSTRAINT [FK_FileLog_Job] FOREIGN KEY ([JobKey]) REFERENCES [internal].[Job] ([JobKey])
);

