﻿

CREATE TABLE [internal].[EmailStatus](
	[EmailStatusKey] [int] NOT NULL,
	[EmailStatusName] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[EmailStatusKey] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) 
) 
GO


