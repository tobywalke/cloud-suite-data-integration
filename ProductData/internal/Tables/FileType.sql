﻿CREATE TABLE [internal].[FileType] (
    [FileTypeKey]          INT     IDENTITY (1, 1)       NOT NULL,
	[DataTypeKey]			INT NOT NULL ,
    [FileTypeName]         NVARCHAR (50)  NOT NULL,
    [StagingProcedureName] NVARCHAR (100) NOT NULL,
    [FormatFile]           NVARCHAR (100) NOT NULL,
    [BlobFolderName]       NVARCHAR (100) NULL,
	[SchemaFileName]       NVARCHAR (250) NULL,
    PRIMARY KEY CLUSTERED ([FileTypeKey] ASC)
);

