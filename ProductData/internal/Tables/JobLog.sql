﻿CREATE TABLE [internal].[JobLog] (
    [JobLogKey] BIGINT         IDENTITY (1, 1) NOT NULL,
    [JobKey]    BIGINT         NOT NULL,
    [Procedure] NVARCHAR (200) NULL,
    [Section]   VARCHAR (2048) NULL,
    [StepNo]    INT            NULL,
    [StartTime] DATETIME       NOT NULL,
    [EndTime]   DATETIME       NULL,
    [Duration]  AS             (datediff(millisecond,[StartTime],[EndTime])) PERSISTED,
    [Completed] BIT            CONSTRAINT [DF_JobLog_Completed] DEFAULT ((0)) NULL,
    [ErrorKey]  BIGINT         NULL,
    [Age]       AS             (datediff(day,[EndTime],getdate())),
    CONSTRAINT [PK_JobLog] PRIMARY KEY CLUSTERED ([JobLogKey] ASC),
    CONSTRAINT [FK_JobLog_ErrorKey] FOREIGN KEY ([ErrorKey]) REFERENCES [internal].[Error] ([ErrorKey]),
    CONSTRAINT [FK_JobLog_JobKey] FOREIGN KEY ([JobKey]) REFERENCES [internal].[Job] ([JobKey])
);

