﻿CREATE TABLE [internal].[DataTypeAttributes]
(
	[DataTypeAttributeKey] INT IDENTITY (1, 1)NOT NULL PRIMARY KEY
,	[DataTypeKey] INT NOT NULL 
,	[PubSubscription] NVARCHAR(255) NOT NULL
,	[KeyField] NVARCHAR(255) NOT NULL
,	[Datefield] NVARCHAR(50) NOT NULL
,	[DateFormat] NVARCHAR(50) NOT NULL
,	[FolderLevel1] NVARCHAR(50) NOT NULL
,	[FolderLevel2] NVARCHAR(50) NOT NULL
,	[FolderLevel3] NVARCHAR(50) NOT NULL
)
