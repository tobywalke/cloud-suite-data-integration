﻿CREATE TABLE [internal].[Configuration] (
    [ConfigurationKey]      INT            IDENTITY (1, 1) NOT NULL,
    [ETLInProgress]         NVARCHAR (3)   NULL,
    [TruncateDataWarehouse] NVARCHAR (3)   NULL,
    [ETLLoadDateTime]       DATETIME2 (2)  NULL,
    [CalendarStartDate]     DATE           NULL,
    [JobLogRetentionDays]   INT            NULL,
    [InitialUserLoad]       NVARCHAR (3)   NULL,
    [LatestTransactionDate] DATE           NULL,
    [SystemStartDate]       DATE           NULL,
    [NewDataAvalaible]      CHAR (3)       NULL,
    [AzureFileStorage]      NVARCHAR (500) NULL,
    [StagingStatus]         CHAR (5)       NULL,
    [EmailProfile]          NVARCHAR (50)  NULL,
    [EmailRecipients]       NVARCHAR (500) NULL,
    [DataEmailProfile]      NVARCHAR (50)  NULL,
    [DataEmailRecipients]   NVARCHAR (500) NULL,
    [TruncateStagingFlag]   CHAR (3)       NULL,
    CONSTRAINT [PK_Configuration] PRIMARY KEY CLUSTERED ([ConfigurationKey] ASC)
);

