﻿CREATE TABLE [internal].[FileFailure] (
    [FileFailureKey]  BIGINT         NOT NULL,
    [FileFailureName] NVARCHAR (100) NOT NULL,
    PRIMARY KEY CLUSTERED ([FileFailureKey] ASC)
);

