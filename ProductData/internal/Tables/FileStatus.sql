﻿CREATE TABLE [internal].[FileStatus] (
    [FileStatusKey]  INT            NOT NULL,
    [FileStatusName] NVARCHAR (100) NOT NULL,
    PRIMARY KEY CLUSTERED ([FileStatusKey] ASC)
);

