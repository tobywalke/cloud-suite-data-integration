﻿CREATE TABLE [internal].[Error] (
    [ErrorKey]        BIGINT          IDENTITY (1, 1) NOT NULL,
    [ErrorSource]     NVARCHAR (50)   NULL,
    [ErrorNumber]     INT             NULL,
    [ErrorMessage]    NVARCHAR (4000) NULL,
    [ErrorState]      INT             NULL,
    [ErrorSeverity]   INT             NULL,
    [Routine]         NVARCHAR (128)  NULL,
    [LineNo]          INT             NULL,
    [DatabaseName]    NVARCHAR (128)  NULL,
    [ApplicationName] NVARCHAR (128)  NULL,
    [UserName]        NVARCHAR (128)  NULL,
    [HostName]        NVARCHAR (50)   NULL,
    [Package]         NVARCHAR (128)  NULL,
    [PackageStep]     NVARCHAR (128)  NULL,
    [DateCreated]     SMALLDATETIME   NULL,
    [EmailSent]       BIT             NULL,
    CONSTRAINT [PK_Error] PRIMARY KEY CLUSTERED ([ErrorKey] ASC)
);

