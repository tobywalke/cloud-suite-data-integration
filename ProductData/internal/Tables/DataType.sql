﻿CREATE TABLE [internal].[DataType]
(
	[DataTypeKey] INT IDENTITY (1, 1)NOT NULL PRIMARY KEY
,	[DataGroupKey]		INT NOT NULL 
,	[DataTypeName]		NVARCHAR(50) NOT NULL
,	[DataTypeSource]	NVARCHAR(50) NOT NULL
,	[DataTypeFormat]	NVARCHAR(50) NOT NULL
,	[DataTypeVelocity]	NVARCHAR(50) NOT NULL

)
