﻿CREATE TABLE [internal].[Job] (
    [JobKey]                            BIGINT        IDENTITY (1, 1) NOT NULL,
    [JobStatus]                         VARCHAR (50)  NOT NULL,
    [IsRunning]                         BIT           NOT NULL,
    [StartDateTime]                     DATETIME2 (2) NOT NULL,
    [StageLoadCompleteDateTime]         DATETIME2 (2) NULL,
    [DataWarehouseLoadCompleteDateTime] DATETIME2 (2) NULL,
    [DataModelProcessCompleteDateTime]  DATETIME2 (2) NULL,
    [FinishDateTime]                    DATETIME2 (2) NULL,
    CONSTRAINT [PK_Job] PRIMARY KEY CLUSTERED ([JobKey] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCI_Job_IsRunning]
    ON [internal].[Job]([IsRunning] ASC, [JobKey] ASC) WHERE ([IsRunning]=(1));

