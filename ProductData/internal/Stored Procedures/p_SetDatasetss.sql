﻿

CREATE PROCEDURE [internal].[p_SetDatasets]
AS

--==================================================================================================================
--Author:		Ricoh
--Created date: January 2018
--Description:	Lists out datasets for deployment
-- ------------------------------------------------------------------------------------------------------------------
--Revision History:
--	Date			Person					Change
--	08/02/18		Jon Lunn				Added Sage File Types
-- ------------------------------------------------------------------------------------------------------------------
--Debug Code:

--==================================================================================================================

BEGIN
    SET XACT_ABORT ON							-- Keyword to enable the XACT Transaction Handling
    SET NOCOUNT ON								-- Eliminate any dataset counts

    DECLARE @returnValue		INT = 0			-- Defaults to returning 0
    ,		@initialJobLogKey	INT				-- Stores the initial job log key to use when an exception occcurs
	,		@jobKey				INT				-- Identity for Job Table
	,		@jobLogKey			INT				-- Identity for Job Log Table
    ,		@stepNo				INT = 1			-- unique step counter 
    ,		@section			NVARCHAR(255)	-- document your steps by setting this variable

	
    BEGIN TRY

		BEGIN TRANSACTION


		DECLARE @Count INT, @RowCount INT
		DECLARE @DataGroupName		NVARCHAR(50)  
		,		@DataTypeSource		NVARCHAR(50) 
		,		@DataTypeFormat		NVARCHAR(50)
		,		@DataTypeVelocity	NVARCHAR(50) 
		,		@PubSubscription	NVARCHAR(255) 
		,		@DataTypeName		NVARCHAR(50) 
		,		@KeyField			NVARCHAR(255) 
		,		@Datefield			NVARCHAR(50) 
		,		@DateFormat			NVARCHAR(50) 
		,		@SchemaFileName		NVARCHAR (250) 
		,		@FolderLevel1		NVARCHAR(50) 
		,		@FolderLevel2		NVARCHAR(50) 
		,		@FolderLevel3		NVARCHAR(50) 

		CREATE TABLE #datasets
		(
			[RowNumber]  INT IDENTITY (1, 1) NOT NULL
		,	[DataGroupName] NVARCHAR(50)  NOT NULL 
		,	[DataTypeSource] NVARCHAR(50) NOT NULL
		,	[DataTypeFormat] NVARCHAR(50) NOT NULL
		,	[DataTypeVelocity] NVARCHAR(50) NOT NULL
		,	[PubSubscription] NVARCHAR(255) NOT NULL
		,	[DataTypeName] NVARCHAR(50) NOT NULL
		,	[KeyField] NVARCHAR(255) NOT NULL
		,	[Datefield] NVARCHAR(50) NOT NULL
		,	[DateFormat] NVARCHAR(50) NOT NULL
		,	[SchemaFileName]       NVARCHAR (250) NULL
		,	[FolderLevel1] NVARCHAR(50) NOT NULL
		,	[FolderLevel2] NVARCHAR(50) NOT NULL
		,	[FolderLevel3] NVARCHAR(50) NOT NULL

		)
			INSERT INTO #datasets	
		(	[DataGroupName] 
		 ,	[DataTypeSource] 
		 ,	[DataTypeFormat] 
		 ,	[DataTypeVelocity]
		 ,	[PubSubscription] 
		 ,	[DataTypeName] 
		 ,	[KeyField] 
		 ,	[Datefield] 
		 ,	[DateFormat] 
		 ,	[SchemaFileName]  
		 ,	[FolderLevel1]
		 ,	[FolderLevel2]
		 ,	[FolderLevel3]
		 )
		SELECT 'smart_spaces_footfall','cognipoint','json','stream','projects/ricoh-smart-spaces/subscriptions/rds-cognipoint','counting','type','timestamp','unixmill','https://console.cloud.google.com/storage/browser/configdata/countingbqschema.json' ,'v1','customerid','entity'
		UNION 
		SELECT 'smart_spaces_footfall','cognipoint','json','stream','projects/ricoh-smart-spaces/subscriptions/rds-cognipoint','traffic','type','timestamp','unixmill','https://console.cloud.google.com/storage/browser/configdata/trafficbqschema.json' ,'v1','customerid','entity'
		UNION 
		SELECT 'smart_spaces_event','outlook','json','streambatch','projects/ricoh-smart-spaces/subscriptions/juliatest','event','entity','startTime','datetime','https://console.cloud.google.com/storage/browser/configdata/eventbqschema.json' ,'v1','customerid','entity'
		UNION 
		SELECT 'smart_spaces_event','visitorsignin','json','streambatch','projects/ricoh-smart-spaces/subscriptions/visitor-updated','visitorsignin','entity','signInTime','datetime','https://console.cloud.google.com/storage/browser/configdata/visitorsigninbqschema.json' ,'v1','customerid','entity'
		UNION 
		SELECT 'smart_spaces_info','smart spaces','json','streambatch','','floor','','','','' ,'v1','customerid','entity'
		UNION 
		SELECT 'smart_spaces_info','smart spaces','json','streambatch','','meetingroom','','','','' ,'v1','customerid','entity'
		UNION
		SELECT 'smart_spaces_info','smart spaces','json','streambatch','','location','','','','' ,'v1','customerid','entity'
		UNION
		SELECT 'smart_spaces_info','smart spaces','json','streambatch','','customer','','','','' ,'v1','date','entity'
		UNION
		SELECT 'smart_spaces_info','smart spaces','json','streambatch','','user','','','','' ,'v1','date','entity'
	

		SET @Count = 1
		SET @RowCount = (SELECT COUNT(*) FROM #datasets)
		WHILE @Count<= @RowCount
			BEGIN
				SELECT
					@DataGroupName		= [DataGroupName] 
				,	@DataTypeSource		= [DataTypeSource] 
				,	@DataTypeFormat		= [DataTypeFormat] 
				,	@DataTypeVelocity	= [DataTypeVelocity]
				,	@PubSubscription	= [PubSubscription] 
				,	@DataTypeName		= [DataTypeName] 
				,	@KeyField			= [KeyField] 
				,	@Datefield			= [Datefield] 
				,	@DateFormat			= [DateFormat] 
				,	@SchemaFileName		= [SchemaFileName]  
				,	@FolderLevel1		= FolderLevel1
				,	@FolderLevel2		= FolderLevel2
				,	@FolderLevel3		= FolderLevel3

				FROM #datasets
				WHERE	RowNumber = @Count

				EXEC internal.p_InsertDataset @DataGroupName	
											   ,	@DataTypeSource
											   ,	@DataTypeFormat
											   ,	@DataTypeVelocity
											   ,	@PubSubscription
											   ,	@DataTypeName	
											   ,	@KeyField		
											   ,	@Datefield		
											   ,	@DateFormat		
											   ,	@SchemaFileName
											   ,	@FolderLevel1
											   ,	@FolderLevel2
											   ,	@FolderLevel3



				SET @Count = @Count+1
			END



		COMMIT TRANSACTION

    END TRY

    BEGIN CATCH


		--If -1, the transaction is uncommittable and should be rolled back.
		IF ( XACT_STATE() ) = -1
			ROLLBACK TRANSACTION

		-- If 1, the transaction is committable.
		IF ( XACT_STATE() ) = 1
			COMMIT TRANSACTION

		-- Log the error and raise it again
		EXECUTE [internal].[p_LogAndRaiseSQLError] @section = @section, @jobLogKey = @jobLogKey

		SET @returnValue = 1 -- Set different return value as an exception has been generated

    END CATCH

    SET XACT_ABORT OFF

    RETURN @returnValue

END