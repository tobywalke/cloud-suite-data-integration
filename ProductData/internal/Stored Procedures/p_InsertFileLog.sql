﻿CREATE PROCEDURE [internal].[p_InsertFileLog]
		@FilePath			VARCHAR(1000)

AS
BEGIN
-- ==================================================================================================================
-- Author:			Toby Walke
-- Create date:		19/07/2019
-- Description:		Inserts filelog record on first receipt of data file.
-- ------------------------------------------------------------------------------------------------------------------
-- Revision History:

-- ------------------------------------------------------------------------------------------------------------------
-- Debug Code:
/*

*/
-- ==================================================================================================================

	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
	
    DECLARE @returnValue		INT = 0			-- Defaults to returning 0
	,		@jobKey				INT	= -1		-- Identity for Job Table
	,		@jobLogKey			INT	= -1		-- Identity for Job Table
    ,		@stepNo				INT = 1			-- unique step counter 
    ,		@section			NVARCHAR(255)	-- document your steps by setting this variable
	,		@FileName			NVARCHAR(1000)  -- Gets the Blob container name from the File Type Table	
	,		@FileTypeKey		INT
	,		@InsertFileLogKey	INT
	
    BEGIN TRY

		BEGIN TRANSACTION

		-- Log a point in the proceduere through this code 

			
			SET @section = 'Insert into file log'
				
			;WITH cte_datatypes
			AS
			(
				SELECT	dt.DataTypeKey
				,		dt.DataTypeName
				,		'raw/'+ dg.DataGroupName + '/' + ft.FileTypeName	As FolderRoot
				,		ft.FileTypeKey
				,		ft.FileTypeName
				FROM	internal.DataGroup dg
				INNER JOIN
						internal.DataType dt
				ON		dg.DataGroupKey = dt.DataGroupKey
				INNER JOIN
						internal.FileType ft
				ON		dt.DataTypeKey = ft.DataTypeKey
			)
			
			SELECT	@FileName = REVERSE(SUBSTRING(REVERSE(@FilePath),1,CHARINDEX('/',REVERSE(@FilePath))-1)) 
			,		@FileTypeKey = dt.FileTypeKey
			FROM	cte_datatypes dt
			WHERE	@FilePath like  '%' + dt.FolderRoot + '%';


			WITH cte_file
			AS
			(
				SELECT	@FileName AS [Filename]
				,		@FileTypeKey AS FileTypeKey
			)

			MERGE 
				[internal].[FileLog] AS TARGET
			USING 
				cte_file
			ON
				TARGET.[FileName]		= cte_file.[Filename]
			AND	TARGET.FileTypeKey		= cte_file.FileTypeKey
			WHEN MATCHED THEN
			UPDATE
				SET 
					FileStatusKey = 1
			,		EmailStatusKey = 1
			WHEN NOT MATCHED BY TARGET THEN
			INSERT
			(		[JobKey]
			,		[FilePath]
			,		[FileName]
			,		[OriginalFileName]
			,		[FileTypeKey]
			,		[FileStatusKey]
			,		[EmailStatusKey]
			)
			VALUES
			(	
					@jobKey
			,		@FilePath
			,		@FileName
			,		@FileName
			,		@FileTypeKey
			,		1
			,		1
			);



			
		COMMIT TRANSACTION

	END TRY

	BEGIN CATCH
		-- Test XACT_STATE for 0, 1, or -1.
		-- If 1, the transaction is committable.
		-- If -1, the transaction is uncommittable and should 
		--     be rolled back.
		-- XACT_STATE = 0 means there is no transaction and
		--     a commit or rollback operation would generate an error.

		IF (XACT_STATE()) = -1
			ROLLBACK TRANSACTION
 
		IF (XACT_STATE()) = 1
			COMMIT TRANSACTION

		-- log error
		EXECUTE [internal].[p_LogAndRaiseSQLError] @section = @section, @jobLogKey = @jobLogKey

		SET @section = 'Hit an error'
		EXEC [internal].[p_InsertJobLog] @section = @section, @stepNo = @stepNo OUTPUT, @jobLogKey = @jobLogKey OUTPUT;

		RETURN 1 -- return error as an exception has been generated!
	END CATCH
END