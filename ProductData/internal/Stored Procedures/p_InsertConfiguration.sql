﻿
CREATE PROCEDURE [internal].[p_InsertConfiguration]
AS

--==================================================================================================================
--Author:		Ricoh
--Created date: 13/11/2015
--Description:	Creates a default configuration record if none already exist
-- ------------------------------------------------------------------------------------------------------------------
--Revision History:
--	Date			Person					Change

-- ------------------------------------------------------------------------------------------------------------------
--Debug Code:

--	EXEC [internal].[p_InsertConfiguration]
--	SELECT * FROM internal.Configuration
--	SELECT * FROM internal.Error ORDER BY DateCreated DESC

--==================================================================================================================

BEGIN
    SET NOCOUNT ON								-- Eliminate any dataset counts

    DECLARE @returnValue		INT = 0			-- Defaults to returning 0
	,		@jobKey				INT				-- Identity for Job Table
	,		@JobLogKey			INT				-- Identity for Job Log Table
    ,		@stepNo				INT = 1			-- unique step counter 
    ,		@section			NVARCHAR(255)	-- document your steps by setting this variable

	
    BEGIN TRY

		 
		SET @section = 'Insert configuration record'
		TRUNCATE TABLE internal.Configuration
		
		INSERT INTO	internal.Configuration
		(
			[ETLInProgress]
		,	[TruncateDataWarehouse]
		,	[ETLLoadDateTime]
		,	[CalendarStartDate]
		,	[JobLogRetentionDays]
		,	[InitialUserLoad]
		,	[LatestTransactionDate]
		,	[SystemStartDate]
		,	[AzureFileStorage]
		,	[StagingStatus]
		,	[EmailProfile]
		,	[EmailRecipients]
		,	[DataEmailProfile]
		,	[DataEmailRecipients]
		,	[TruncateStagingFlag]
		)
		SELECT
			'No'						AS ETLInProgress
		,	'No'						AS TruncateDataWarehouse
		,	'01/01/2018'				AS ETLLoadDateTime
		,	'01/01/2018'				AS CalendarStartDate
		,	60							AS JobLogRetentionDays
		,	'Yes'						AS InitialUserLoad
		,	NULL						AS LatestTransactionDate
		,	'01/01/2016'				AS SystemStartDate
		,	'prstortst01'				AS AzureFileStorage
		,	'N/A'						AS StagingStatus
		,	'Unspecified'				AS EmailProfile
		,	'toby.walke@ricoh.co.uk'	AS EmailRecipients
		,	'Unspecified'				AS DataEmailProfile
		,	'toby.walke@ricoh.co.uk'	AS DataEmailRecipients
		,	'Yes'						AS TruncateStagingFlag
		WHERE NOT EXISTS (SELECT 1 FROM internal.Configuration)

    END TRY

    BEGIN CATCH

		-- Log the error and raise it again
		

		SET @returnValue = 1 -- Set different return value as an exception has been generated

    END CATCH


    RETURN @returnValue

END