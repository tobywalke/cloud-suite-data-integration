﻿CREATE PROCEDURE [internal].[p_SendEmailStageSummary] 
AS


DECLARE @FontStyle		VARCHAR(100)
,		@SubjectText	VARCHAR(150)
,		@BodyText		VARCHAR(8000)
,		@CustomBody		VARCHAR(8000)
,		@FileTable		VARCHAR(MAX)
,		@ErrorTable		VARCHAR(MAX)
,		@FileBody		VARCHAR(MAX)
,		@ErrorBody		VARCHAR(MAX)
,		@ErrrorCount	int
,		@EmailRecipients VARCHAR(500)

SET @FileTable = CAST((
		SELECT		[FileName] AS 'td', ''
		,			isnull(FileLoadDate,'') AS 'td', ''
		,			isnull(FileTypeName,'') AS 'td', ''
		,			isnull(FileStatusName,'') AS 'td', ''
		FROM 		[internal].v_FileLog
		WHERE		EmailStatusKey = 1
		ORDER BY	FileLoadDate
		FOR XML PATH('tr'), ELEMENTS) AS NVARCHAR(MAX))

SET @FileBody = 
			'<p> the Following files have been processed</p>'
		+	'<p></p>'
		+	'<h1>File Load Summary</h1>'
		+	'<table border="1" cellpadding="5" cellspacing="0"">'
		+	'<tr><th>File Name</th><th>File Load Date</th><th>File Type</th><th>File Status</th></tr>'		
		+	isnull(@FileTable,'') +'</table>'

SET @ErrrorCount = (SELECT COUNT(*)
					FROM	[internal].[Error]
					WHERE	[EmailSent] = 0)
SET @ErrorTable = CAST((
		SELECT	ISNULl([ErrorMessage],'')		AS 'td', ''
		,		ISNULl([Routine],'')			AS 'td', ''
		,		ISNULl([LineNo]	,'')			AS 'td', ''
		,		ISNULl([DatabaseName],'')		AS 'td', ''  
		FROM	[internal].[Error]
		WHERE	[EmailSent] = 0
		--AND		[Routine] like 'stage%'	
		FOR XML PATH('tr'), ELEMENTS) AS NVARCHAR(MAX))

SET @ErrorBody = 
			'<p> Please investigate the issue using the details below or contact Ricoh for further assistance.</p>'
		+	'<p></p>'
		+	'<h1>Error Summary</h1>'
		+	'<table border="1" cellpadding="5" cellspacing="0"">'
		+	'<tr><th>Error Message</th><th>Routine</th><th>Line No</th><th>Database</th></tr>'		
		+	isnull(@ErrorTable,'') +'</table>'

-- Generate HTML for email body text
SET @FontStyle = '{font-family: Verdana; font-size: 0.7em; text-align: left;}'

SET @BodyText	= 
			'<html>'
		+	'<head>'
		+	'<style type="text/css"> h1 ' + @FontStyle + ' p ' + @FontStyle + ' th ' + @FontStyle + ' td ' + @FontStyle + ' </style>'
		+	'</head>'
		+	'<body>'

-- Build the email body message
SELECT @CustomBody	= CASE	WHEN @ErrrorCount >0
							THEN  '<p>The BI ETL FAILED during the last run on ' + @@SERVERNAME +'.</p>'
									+ @FileBody
									+'<p></p>'
									+ @ErrorBody
							ELSE  '' END
					+ CASE	WHEN @ErrrorCount = 0 
							THEN '<p>The BI ETL succeeded during the last run on ' + @@SERVERNAME + '.</p>'	
								+ @FileBody
								+'<p></p>'
								
							ELSE '' END
					;

---- Build an error table if necessary
SET @BodyText = @BodyText + @CustomBody 
		
-- Complete the HTML and encode it properly
--SET @BodyText = @BodyText + @CustomBody + @ErrorTable + '</body></html>'
SET @BodyText = @BodyText +  '</body></html>'
SET @BodyText = REPLACE(REPLACE(@BodyText,'&lt;','<'),'&gt;','/>')		--correct xml brackets
SET @BodyText = REPLACE(REPLACE(@BodyText, CHAR(13), ''), CHAR(10), '') --remove CR and LF
SET @BodyText = REPLACE(@BodyText, '''', '')							--remove single quotes to allow
SET @EmailRecipients = (SELECT EmailRecipients FROM internal.[Configuration])

SELECT @BodyText AS EmailBody
, @EmailRecipients AS EmailRecipients

UPDATE internal.FileLog
SET EmailStatusKey = 2
WHERE EmailStatusKey = 1

UPDATE internal.Error
SET EmailSent = 1
WHERE EmailSent = 0

RETURN