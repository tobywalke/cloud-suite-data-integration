﻿CREATE PROCEDURE [internal].[p_TruncateTablesInSchema]
(
	@SchemaToTruncate NVARCHAR(100) 
,	@SchemaToIgnore NVARCHAR(100) = ''
,	@debug BIT = 0
)
AS

--==================================================================================================================
--Author:		Ridgian
--Created date: 13/11/2015
--Description:	Truncates all tables in the specified schema. Mainly used for staging tables as script does not
--				handle tables with foreign key constraints
-- ------------------------------------------------------------------------------------------------------------------
--Revision History:
--	Date			Person					Change

-- ------------------------------------------------------------------------------------------------------------------
--Debug Code:

--	EXEC [internal].[p_TruncateTablesInSchema]
--	SELECT * FROM internal.Error ORDER BY DateCreated DESC

--==================================================================================================================

BEGIN
    SET NOCOUNT ON								-- Eliminate any dataset counts

    DECLARE @returnValue		INT = 0			-- Defaults to returning 0
    ,		@initialJobLogKey	INT				-- Stores the initial job log key to use when an exception occcurs
	,		@jobKey			int				-- Identity for Job Table
	,		@JobLogKey			INT				-- Identity for Job Log Table
    ,		@stepNo				INT = 1			-- unique step counter 
    ,		@section			NVARCHAR(255)	-- document your steps by setting this variable

		-- Log the start time of the Procedure
    EXEC [internal].[p_InsertJobLog] @procId = @@PROCID, @jobKey = @jobKey OUTPUT, @jobLogKey = @jobLogKey OUTPUT;
	
    BEGIN TRY

		SET @section = 'Truncate tables in specified schema' 
		EXEC [internal].[p_InsertJobLog] @section = @section, @stepNo = @stepNo OUTPUT, @jobLogKey = @jobLogKey OUTPUT;	
		
		DECLARE @v_SchemaToTruncate NVARCHAR(100)
		,		@v_SchemaToIgnore	NVARCHAR(100)
		,		@v_TableToTruncate	NVARCHAR(200)
		,		@v_SQLtoExecute		NVARCHAR(MAX)
		,		@v_ID				INT
		,		@v_MaxID			INT
		,		@TruncateFlag		char(3) = (SELECT TruncateStagingFlag FROM internal.[Configuration])
		,		@SQL varchar(max)
		-- Table variable
		DECLARE	@Table TABLE 
							(	ID INT
							,	TABLE_SCHEMA nvarchar(100)
							,	TABLE_NAME nvarchar(100)
							)

		SET @v_SchemaToTruncate = @SchemaToTruncate
		SET @v_SchemaToIgnore = @SchemaToIgnore

		

		IF @TruncateFlag = 'Yes'
		BEGIN
			IF @SchemaToIgnore != ''
				BEGIN
					SET @SQL =
					'
					SELECT	ROW_NUMBER() OVER (ORDER BY TABLE_NAME) AS ID
					,		TABLE_SCHEMA
					,		TABLE_NAME
					FROM	INFORMATION_SCHEMA.TABLES
					WHERE	TABLE_SCHEMA like ''' +@v_SchemaToTruncate+'%''
					AND		TABLE_TYPE = ''BASE TABLE''
					AND		TABLE_SCHEMA NOT LIKE ''' + @v_SchemaToIgnore+'%'''
				END
			ELSE
			BEGIN
					SET @SQL =
					'
					SELECT	ROW_NUMBER() OVER (ORDER BY TABLE_NAME) AS ID
					,		TABLE_SCHEMA
					,		TABLE_NAME
					FROM	INFORMATION_SCHEMA.TABLES
					WHERE	TABLE_SCHEMA like ''' +@v_SchemaToTruncate+'%''
					AND		TABLE_TYPE = ''BASE TABLE'''
				END
			
			--print @sql
			INSERT INTO @Table
			EXEC(@Sql)

			SELECT @v_ID = MIN(ID) FROM @Table
			SELECT @v_MaxID = MAX(ID) FROM @Table

			WHILE @v_ID <= @v_MaxID
			BEGIN
				SELECT @v_TableToTruncate = TABLE_NAME FROM @Table WHERE ID = @v_ID
				SELECT @v_SchemaToTruncate = TABLE_SCHEMA FROM @Table WHERE ID = @v_ID

				SET @v_SQLtoExecute = '
					TRUNCATE TABLE [' + @v_SchemaToTruncate + '].[' + @v_TableToTruncate + ']'
		
				IF @debug = 1 
					PRINT @v_SQLtoExecute
				ELSE
					EXEC sp_executesql @v_SQLtoExecute
			
				SET @v_ID = @v_ID + 1
			END
		END

    END TRY

    BEGIN CATCH

		-- Log the error and raise it again
		EXECUTE [internal].[p_LogAndRaiseSQLError] @section = @section, @JobLogKey = @JobLogKey

		SET @returnValue = 1 -- Set different return value as an exception has been generated

    END CATCH

    -- Log the end of the Procedure Run, success or otherwise    
    EXEC [internal].[p_InsertJobLog] @JobLogKey = @JobLogKey

	RETURN @returnValue

END