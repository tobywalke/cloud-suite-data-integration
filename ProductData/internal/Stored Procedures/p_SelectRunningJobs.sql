﻿
CREATE PROCEDURE [internal].[p_SelectRunningJobs]
AS
BEGIN

	/* ===============================================================================================================
	Author:			Carl Follows
	Create date:	11/07/2017
	Description:	Select the details of new Job execution 
	------------------------------------------------------------------------------------------------------------------
	Debug Code:
	

	=============================================================================================================== */

    SET NOCOUNT ON							-- Eliminate any dataset counts

    DECLARE @returnValue	int = 0			-- Defaults to returning 0
	,		@jobKey			int				-- Identity for Job Table
	,		@jobLogKey		int				-- Identity for Job Log Table
    ,		@stepNo			int = 1			-- unique step counter 
    ,		@section		nvarchar(2048)	-- document your steps by setting this variable
	
    BEGIN TRY

			SELECT	JobKey
			FROM	[internal].[Job]
			WHERE	[IsRunning] = 1
			AND		DATEDIFF(hour, StartDateTime, GetDate()) < 3; -- Allow max 3 hours before abandoning previous job.

    END TRY

    BEGIN CATCH

		-- Log error the error and raise it again
		EXECUTE [internal].[p_LogAndRaiseSQLError] @section = @section, @jobLogKey = @jobLogKey

		SET @returnValue = 1 -- Set different return value as an exception has been generated

    END CATCH

    RETURN @returnValue

END