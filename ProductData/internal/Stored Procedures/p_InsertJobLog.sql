﻿CREATE PROCEDURE [internal].[p_InsertJobLog]
	@section    varchar(2048)	= NULL
,	@procId		int				= NULL
,	@stepNo		int				= -1	OUTPUT
,	@jobKey		int				= NULL	OUTPUT
,	@jobLogKey	int						OUTPUT
AS

--==================================================================================================================
--Author:		Dave McMahon		
--Created date: 03/11/2015
--Description:	Stored procedure to insert records into internal.JobLog
-- ------------------------------------------------------------------------------------------------------------------
--Revision History:
--	Date			Person					Change

-- ------------------------------------------------------------------------------------------------------------------
--Debug Code:

--	DECLARE @RC int
--	DECLARE @section varchar(255)
--	DECLARE @procId int
--	DECLARE @stepNo int
--	DECLARE @jobLogKey int

---- TODO: Set parameter values here.

--	EXECUTE @RC = [internal].[InsertJobLog] 
--	@section
--	,@procId
--	,@stepNo OUTPUT
--  ,@jobLogKey OUTPUT
--	GO
--==================================================================================================================

BEGIN

	SELECT	@jobKey = ISNULL(MAX(JobKey), -1) --should only be 1, but just in case (the creation of a new job will be logged as -1)
	FROM	[internal].[Job]
	WHERE	[IsRunning] = 1

	-- Deal with a new procedure 
	IF ( @stepNo = -1 ) AND ( @jobLogKey IS NULL )
	BEGIN     
		SET @stepNo = 1

		INSERT INTO [internal].[JobLog]
		(	[JobKey]
		,	[Procedure]
		,	[Section]
		,	[StepNo]
		,	[StartTime]
		)
		VALUES
		(	@JobKey
		,	OBJECT_SCHEMA_NAME(@procId) + '.' + OBJECT_NAME(@procId)
		,	'Starting'
		,	@stepNo
		,	GETDATE()
		)

		-- Get the IDENTITY value 
		SET @jobLogKey = SCOPE_IDENTITY()

		RETURN 0
	END

    -- Next deal with the finishing up of the procedure
    IF ( @stepNo = -1 ) AND ( @jobLogKey IS NOT NULL )
    BEGIN
        UPDATE	[internal].[JobLog]
        SET		[EndTime] = GETDATE()
        ,		[Section] = CASE WHEN @section IS NULL THEN [Section] ELSE @section END
        ,		[Completed] = 1
        WHERE	[JobLogKey] = @jobLogKey
        AND		[EndTime] IS NULL

        RETURN 0
    END

    -- Next deal with an intermediate step  
    IF ( @stepNo > 0 )
	BEGIN
		UPDATE	[internal].[JobLog]
		SET		[EndTime] = GETDATE()
		,		[Completed] = 1
		WHERE	[JobLogKey] = @jobLogKey

		SET @stepNo = @stepNo + 1

		INSERT INTO [internal].[JobLog]
		(		[JobKey]
		,		[Procedure]
		,		[Section]
		,		[StepNo]
		,		[StartTime]
		)
		SELECT	@JobKey
		,		[Procedure]
		,		@section
		,		@stepNo
		,		GETDATE()
		FROM	[internal].[JobLog]
		WHERE	[JobLogKey] = @jobLogKey

		-- Get the IDENTITY value
		SET @jobLogKey = SCOPE_IDENTITY()

		RETURN 0
    END
END