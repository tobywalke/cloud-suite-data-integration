﻿
CREATE PROCEDURE [internal].[p_SelectJob]
	@RequestedJobKey bigint
AS
BEGIN

	/* ===============================================================================================================
	Author:			Carl Follows
	Create date:	11/07/2017
	Description:	Select the details of new Job execution 
	------------------------------------------------------------------------------------------------------------------
	Debug Code:
	

	=============================================================================================================== */

    SET NOCOUNT ON							-- Eliminate any dataset counts

    DECLARE @returnValue	int = 0			-- Defaults to returning 0
	,		@jobKey			bigint				-- Identity for Job Table
	,		@jobLogKey		bigint				-- Identity for Job Log Table
    ,		@stepNo			int = 1			-- unique step counter 
    ,		@section		nvarchar(2048)	-- document your steps by setting this variable
	
    BEGIN TRY

			SELECT	[JobKey]
            ,		[JobStatus]
            ,		[IsRunning]
            ,		[StartDateTime]
            ,		[StageLoadCompleteDateTime]
            ,		[DataWarehouseLoadCompleteDateTime]
			,		[DataModelProcessCompleteDateTime]
            ,		[FinishDateTime]
			FROM	[internal].[Job]
			WHERE	[JobKey] = @RequestedJobKey;

    END TRY

    BEGIN CATCH

		-- Log error the error and raise it again
		EXECUTE [internal].[p_LogAndRaiseSQLError] @section = @section, @jobLogKey = @jobLogKey

		SET @returnValue = 1 -- Set different return value as an exception has been generated

    END CATCH

    RETURN @returnValue

END