﻿

CREATE PROCEDURE [internal].[p_InsertDataset]
(
		@DataGroupName		NVARCHAR(50)  
,		@DataTypeSource		NVARCHAR(50) 
,		@DataTypeFormat		NVARCHAR(50)
,		@DataTypeVelocity	 NVARCHAR(50) 
,		@PubSubscription	NVARCHAR(255) 
,		@DataTypeName		NVARCHAR(50) 
,		@KeyField			NVARCHAR(255) 
,		@Datefield			NVARCHAR(50) 
,		@DateFormat			NVARCHAR(50) 
,		@SchemaFileName		NVARCHAR (250) 
,		@FolderLevel1		NVARCHAR(50) 
,		@FolderLevel2		NVARCHAR(50) 
,		@FolderLevel3		NVARCHAR(50) 
)
AS

--==================================================================================================================
--Author:		Ricoh
--Created date: January 2018
--Description:	Populates internal dataset tables
-- ------------------------------------------------------------------------------------------------------------------
--Revision History:

-- ------------------------------------------------------------------------------------------------------------------
--Debug Code:


--==================================================================================================================

BEGIN
    SET XACT_ABORT ON							-- Keyword to enable the XACT Transaction Handling
    SET NOCOUNT ON								-- Eliminate any dataset counts

    DECLARE @returnValue		INT = 0			-- Defaults to returning 0
    ,		@initialJobLogKey	INT				-- Stores the initial job log key to use when an exception occcurs
	,		@jobKey				INT				-- Identity for Job Table
	,		@jobLogKey			INT				-- Identity for Job Log Table
    ,		@stepNo				INT = 1			-- unique step counter 
    ,		@section			NVARCHAR(255)	-- document your steps by setting this variable

	
    BEGIN TRY

		BEGIN TRANSACTION

		DECLARE @GroupKey INT, @DataTypeKey INT, @AttributeKey INT, @FileKey INT
		
		SET @GroupKey = (	SELECT	dg.DataGroupKey
							FROM	internal.DataGroup dg
							INNER JOIN
									internal.DataType dt
							ON		dg.DataGroupKey = dt.DataGroupKey
							WHERE	dg.DataGroupName = LTRIM(RTRIM(@DataGroupName))
							AND		dt.DataTypeName = LTRIM(RTRIM(@DataTypeName))
						)

		IF @GroupKey IS NULL
			BEGIN
				IF @DataGroupName NOT IN (SELECT DataGroupName FROM internal.DataGroup)
				BEGIN
					INSERT INTO internal.DataGroup
					SELECT	@DataGroupName

					SET @GroupKey = SCOPE_IDENTITY()
				END
				ELSE
					SET @GroupKey = (SELECT DataGroupKey FROM internal.DataGroup WHERE DataGroupName = @DataGroupName)
									   
				INSERT INTO internal.DataType
				SELECT	@GroupKey
				,		LTRIM(RTRIM(@DataTypeName))
				,		LTRIM(RTRIM(@DataTypeSource))
				,		LTRIM(RTRIM(@DataTypeFormat))
				,		LTRIM(RTRIM(@DataTypeVelocity))

				SET @DataTypeKey = SCOPE_IDENTITY()

				INSERT INTO internal.DataTypeAttributes
				SELECT	@DataTypeKey
				,		LTRIM(RTRIM(@PubSubscription))
				,		LTRIM(RTRIM(@KeyField))
				,		LTRIM(RTRIM(@Datefield))
				,		LTRIM(RTRIM(@DateFormat))
				,		LTRIM(RTRIM(@FolderLevel1))
				,		LTRIM(RTRIM(@FolderLevel2))
				,		LTRIM(RTRIM(@FolderLevel3))

				SET @AttributeKey = SCOPE_IDENTITY()

				INSERT INTO internal.FileType
				(
					[DataTypeKey]			
				,	[FileTypeName]        
				,	[StagingProcedureName]
				,	[FormatFile]          
				,	[BlobFolderName]      
				,	[SchemaFileName]  
				)
				SELECT	@DataTypeKey
				,		LTRIM(RTRIM(@DataTypeName))
				,		'TBC'
				,		'TBC'
				,		LTRIM(RTRIM(@DataTypeName))
				,		LTRIM(RTRIM(@SchemaFileName))

			END
		ELSE
			BEGIN
				SET @DataTypeKey = (	SELECT	dt.DataTypeKey
							FROM	internal.DataGroup dg
							INNER JOIN
									internal.DataType dt
							ON		dg.DataGroupKey = dt.DataGroupKey
							WHERE	dg.DataGroupName = LTRIM(RTRIM(@DataGroupName))
							AND		dt.DataTypeName = LTRIM(RTRIM(@DataTypeName))
						)

				UPDATE internal.DataType
				SET 	DataTypeSource = LTRIM(RTRIM(@DataTypeSource))
				,		DataTypeFormat =  LTRIM(RTRIM(@DataTypeFormat))
				,		DataTypeVelocity= LTRIM(RTRIM(@DataTypeVelocity))
				WHERE	DataTypeKey = @DataTypeKey
		
				UPDATE internal.DataTypeAttributes
				SET		PubSubscription =  LTRIM(RTRIM(@PubSubscription))
				,		KeyField= LTRIM(RTRIM(@KeyField))
				,		Datefield =  LTRIM(RTRIM(@Datefield))
				,		[DateFormat] = LTRIM(RTRIM(@DateFormat))
				,		FolderLevel1 = LTRIM(RTRIM(@FolderLevel1))
				,		FolderLevel2 = LTRIM(RTRIM(@FolderLevel2))
				,		FolderLevel3 = LTRIM(RTRIM(@FolderLevel3))
				WHERE	DataTypeKey = @DataTypeKey

				UPDATE  internal.FileType
				SET		FileTypeName =  LTRIM(RTRIM(@DataTypeName))
				,		BlobFolderName =  LTRIM(RTRIM(@DataTypeName))
				,		SchemaFileName =  LTRIM(RTRIM(@SchemaFileName))
				WHERE	DataTypeKey = @DataTypeKey
			END


		COMMIT TRANSACTION

    END TRY

    BEGIN CATCH


		--If -1, the transaction is uncommittable and should be rolled back.
		IF ( XACT_STATE() ) = -1
			ROLLBACK TRANSACTION

		-- If 1, the transaction is committable.
		IF ( XACT_STATE() ) = 1
			COMMIT TRANSACTION

		-- Log the error and raise it again
		EXECUTE [internal].[p_LogAndRaiseSQLError] @section = @section, @jobLogKey = @jobLogKey

		SET @returnValue = 1 -- Set different return value as an exception has been generated

    END CATCH

    SET XACT_ABORT OFF

    RETURN @returnValue

END