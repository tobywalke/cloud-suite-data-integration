﻿

CREATE PROCEDURE [internal].[p_InsertFileStatus]
AS

--==================================================================================================================
--Author:		Ricoh
--Created date: January 2018
--Description:	Populates internal.FileStatus with a list of statuses a file can go through
-- ------------------------------------------------------------------------------------------------------------------
--Revision History:
--	Date			Person					Change

-- ------------------------------------------------------------------------------------------------------------------
--Debug Code:
/*
	SELECT * FROM internal.FileStatus
	DELETE FROM internal.FileStatus
	EXEC [internal].[p_InsertFileStatus]
	SELECT * FROM internal.Error ORDER BY DateCreated DESC
*/
--==================================================================================================================

BEGIN
    SET XACT_ABORT ON							-- Keyword to enable the XACT Transaction Handling
    SET NOCOUNT ON								-- Eliminate any dataset counts

      DECLARE @returnValue		INT = 0			-- Defaults to returning 0
    ,		@initialJobLogKey	INT				-- Stores the initial job log key to use when an exception occcurs
	,		@jobKey				INT				-- Identity for Job Table
	,		@JobLogKey			INT				-- Identity for Job Log Table
    ,		@stepNo				INT = 1			-- unique step counter 
    ,		@section			NVARCHAR(255)	-- document your steps by setting this variable

	
    BEGIN TRY

		BEGIN TRANSACTION

		-- Log a point in the proceduere through this code 
		EXEC [internal].[p_InsertJobLog] @section = @section, @stepNo = @stepNo OUTPUT, @jobLogKey = @jobLogKey OUTPUT;

		SET @section = 'Populate table'
		
		
		;WITH cte_Load AS
		(
					SELECT		-1									AS FileStatusKey
					,			'Unspecified Status'				AS FileStatusName
					UNION ALL
					SELECT		1
					,			'File Ready for Processing'
					UNION ALL
					SELECT		2
					,			'File Processed - Passed'
					UNION ALL
					SELECT		3
					,			'File Processed - Failed'
					UNION ALL
					SELECT		4
					,			'File Skipped'
					UNION ALL
					SELECT		5
					,			'Data Warehouse - Passed'
					UNION ALL
					SELECT		6
					,			'Data Warehouse - Failed'
		)
		MERGE	internal.FileStatus AS t
		USING	cte_load s
		ON		t.FileStatusKey = s.FileStatusKey
		WHEN MATCHED THEN
		UPDATE
		SET		t.FileStatusName = s.FileStatusName
		WHEN NOT MATCHED BY TARGET THEN
		INSERT
		(		FileStatusKey
		,		FileStatusName
		)
		VALUES
		(		FileStatusKey
		,		FileStatusName
		)
		;


		COMMIT TRANSACTION

    END TRY

    BEGIN CATCH

		--If -1, the transaction is uncommittable and should be rolled back.
		IF ( XACT_STATE() ) = -1
			ROLLBACK TRANSACTION

		-- If 1, the transaction is committable.
		IF ( XACT_STATE() ) = 1
			COMMIT TRANSACTION

		-- Log the error and raise it again
		EXECUTE [internal].[p_LogAndRaiseSQLError] @section = @section, @JobLogKey = @JobLogKey

		SET @returnValue = 1 -- Set different return value as an exception has been generated

    END CATCH

    SET XACT_ABORT OFF

    RETURN @returnValue

END