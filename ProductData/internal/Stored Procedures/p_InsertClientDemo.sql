﻿CREATE PROCEDURE [internal].[p_InsertClientDemo]
AS

--==================================================================================================================
--Author:		Ricoh
--Created date: 26/06/2019
--Description:	Creates client demo records 
-- ------------------------------------------------------------------------------------------------------------------
--Revision History:
--	Date			Person					Change

-- ------------------------------------------------------------------------------------------------------------------
--Debug Code:



--==================================================================================================================

BEGIN
    SET NOCOUNT ON								-- Eliminate any dataset counts

    DECLARE @returnValue		INT = 0			-- Defaults to returning 0
	,		@jobKey				INT				-- Identity for Job Table
	,		@JobLogKey			INT				-- Identity for Job Log Table
    ,		@stepNo				INT = 1			-- unique step counter 
    ,		@section			NVARCHAR(255)	-- document your steps by setting this variable

	
    BEGIN TRY

		 
		SET @section = 'Insert client demo record'
		
		TRUNCATE TABLE internal.ClientDemo
		
		INSERT INTO	internal.ClientDemo
		(
			DisplayClientKey
		,	InpersonatingClientKey
		,	IsActive
		)
		-- Inpersonating client nees a record for it self
		SELECT
			1 AS DisplayClientKey
		,	1 AS InpersonatingClientKey
		,	1 AS IsActive
		UNION
		-- Client key of demo user from dim.client table
		SELECT
			6 AS DisplayClientKey
		,	1 AS InpersonatingClientKey
		,	1 AS IsActive

    END TRY

    BEGIN CATCH

		-- Log the error and raise it again
		EXECUTE [internal].[p_LogAndRaiseSQLError] @section = @section, @JobLogKey = @JobLogKey

		SET @returnValue = 1 -- Set different return value as an exception has been generated

    END CATCH


    RETURN @returnValue

END