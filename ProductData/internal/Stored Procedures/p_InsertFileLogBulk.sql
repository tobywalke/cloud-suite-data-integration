﻿

CREATE PROCEDURE [internal].[p_InsertFileLogBulk]

AS

--==================================================================================================================
--Author:		Ricoh
--Created date: January 2018
--Description:	Populates internal dataset tables
-- ------------------------------------------------------------------------------------------------------------------
--Revision History:

-- ------------------------------------------------------------------------------------------------------------------
--Debug Code:


--==================================================================================================================

BEGIN
    SET XACT_ABORT ON							-- Keyword to enable the XACT Transaction Handling
    SET NOCOUNT ON								-- Eliminate any dataset counts

    DECLARE @returnValue		INT = 0			-- Defaults to returning 0
    ,		@initialJobLogKey	INT				-- Stores the initial job log key to use when an exception occcurs
	,		@jobKey				INT				-- Identity for Job Table
	,		@jobLogKey			INT				-- Identity for Job Log Table
    ,		@stepNo				INT = 1			-- unique step counter 
    ,		@section			NVARCHAR(255)	-- document your steps by setting this variable

	
    BEGIN TRY

		BEGIN TRANSACTION

		DECLARE @GroupKey INT, @DataTypeKey INT, @AttributeKey INT, @FileKey INT;
		
		WITH cte_datatypes
		AS
		(
			SELECT	dt.DataTypeKey
			,		dt.DataTypeName
			,		'raw/'+ dg.DataGroupName + '/' + ft.FileTypeName	As FolderRoot
			,		ft.FileTypeKey
			,		ft.FileTypeName
			FROM	internal.DataGroup dg
			INNER JOIN
					internal.DataType dt
			ON		dg.DataGroupKey = dt.DataGroupKey
			INNER JOIN
					internal.FileType ft
			ON		dt.DataTypeKey = ft.DataTypeKey
		)
		INSERT INTO [internal].[FileLog]
           ([JobKey]
           ,[FilePath]
           ,[FileName]
           ,[OriginalFileName]
           ,[FileLoadDate]
           ,[FIleTypeKey]
           ,[FileStatusKey]
           ,[EmailStatusKey])
		SELECT
				-1				AS JobKey
		,		fr.FilePath
		,		REVERSE(SUBSTRING(REVERSE(fr.FilePath),1,CHARINDEX('/',REVERSE(fr.FilePath))-1)) AS [Filename]
		,		REVERSE(SUBSTRING(REVERSE(fr.FilePath),1,CHARINDEX('/',REVERSE(fr.FilePath))-1)) AS OrginalFilename
		,		GETDATE() AS LoadDate
		,		dt.FileTypeKey
		,		1
		,		1
		FROM	internal.FileLogRaw fr
		LEFT JOIN
				internal.FileLog fl
		ON		fr.FilePath = fl.FilePath
		CROSS JOIN cte_datatypes dt
		WHERE	fr.FilePath like  '%' + dt.FolderRoot + '%'
		AND		fl.FilePath  IS NULL
		
		


		COMMIT TRANSACTION

    END TRY

    BEGIN CATCH


		--If -1, the transaction is uncommittable and should be rolled back.
		IF ( XACT_STATE() ) = -1
			ROLLBACK TRANSACTION

		-- If 1, the transaction is committable.
		IF ( XACT_STATE() ) = 1
			COMMIT TRANSACTION

		-- Log the error and raise it again
		EXECUTE [internal].[p_LogAndRaiseSQLError] @section = @section, @jobLogKey = @jobLogKey

		SET @returnValue = 1 -- Set different return value as an exception has been generated

    END CATCH

    SET XACT_ABORT OFF

    RETURN @returnValue

END