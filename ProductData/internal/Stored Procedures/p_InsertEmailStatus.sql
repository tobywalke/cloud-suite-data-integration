﻿

CREATE PROCEDURE [internal].[p_InsertEmailStatus]
AS

--==================================================================================================================
--Author:		Ricoh
--Created date: January 2018
--Description:	Populates internal.EmailStatus with a list of statuses to track emails sent
-- ------------------------------------------------------------------------------------------------------------------
--Revision History:
--	Date			Person					Change

-- ------------------------------------------------------------------------------------------------------------------
--Debug Code:
/*
	SELECT * FROM internal.EmailStatus
	DELETE FROM internal.EmailStatus
	EXEC [internal].[p_InsertEmailStatus]
	SELECT * FROM internal.Error ORDER BY DateCreated DESC
*/
--==================================================================================================================

BEGIN
    SET XACT_ABORT ON							-- Keyword to enable the XACT Transaction Handling
    SET NOCOUNT ON								-- Eliminate any dataset counts

      DECLARE @returnValue		INT = 0			-- Defaults to returning 0
    ,		@initialJobLogKey	INT				-- Stores the initial job log key to use when an exception occcurs
	,		@jobKey				INT				-- Identity for Job Table
	,		@JobLogKey			INT				-- Identity for Job Log Table
    ,		@stepNo				INT = 1			-- unique step counter 
    ,		@section			NVARCHAR(255)	-- document your steps by setting this variable

	
    BEGIN TRY

		BEGIN TRANSACTION
		SET @initialJobLogKey =  @JobLogKey


		SET @section = 'Populate table'

		;WITH cte_Load AS
		(
						SELECT		-1 AS EmailStatusKey
			,			'Unspecified Status' AS EmailStatusName
						UNION
						SELECT		1
			,			'Email Required'
						UNION
						SELECT		2
			,			'Email Sent'
		)
		MERGE	internal.EmailStatus AS t
		USING	cte_load s
		ON		s.EmailStatusKey = t.EmailStatusKey
		WHEN MATCHED THEN
		UPDATE
		SET		t.EmailStatusName = s.EmailStatusName
		WHEN NOT MATCHED BY TARGET THEN
		INSERT
		(		EmailStatusKey
		,		EmailStatusName
		)
		VALUES
		(		EmailStatusKey
		,		EmailStatusName
		)
		;


		COMMIT TRANSACTION

    END TRY

    BEGIN CATCH

		-- XACT_STATE = 0 means there is no transaction and a commit or rollback operation would generate an error.
		-- If there is a transaction we need to obtain the first JobLog key as all other JobLog entries will be rolled back

		--If -1, the transaction is uncommittable and should be rolled back.
		IF ( XACT_STATE() ) = -1
			ROLLBACK TRANSACTION

		-- If 1, the transaction is committable.
		IF ( XACT_STATE() ) = 1
			COMMIT TRANSACTION

		-- Log the error and raise it again
		EXECUTE [internal].[p_LogAndRaiseSQLError] @section = @section, @JobLogKey = @JobLogKey

		SET @returnValue = 1 -- Set different return value as an exception has been generated

    END CATCH

    SET XACT_ABORT OFF

    RETURN @returnValue

END