﻿

CREATE procedure [internal].[p_UpdateFileLog] --'Current Meter Values_205.csv'
(
	@Filename nvarchar(250)
)
AS
BEGIN
-- ==================================================================================================================
-- Author:			Ricoh
-- Create date:		January 2018
-- Description:	
-- ------------------------------------------------------------------------------------------------------------------
-- Revision History:

-- ------------------------------------------------------------------------------------------------------------------
-- Debug Code:
/*

*/
-- ==================================================================================================================

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
      DECLARE @returnValue		INT = 0			-- Defaults to returning 0
    ,		@initialJobLogKey	INT				-- Stores the initial job log key to use when an exception occcurs
	,		@jobKey				INT				-- Identity for Job Table
	,		@JobLogKey			INT				-- Identity for Job Log Table
    ,		@stepNo				INT = 1			-- unique step counter 
    ,		@section			NVARCHAR(255)	-- document your steps by setting this variable

		-- Log the start time of the Procedure
    EXEC [internal].[p_InsertJobLog] @procId = @@PROCID, @jobKey = @jobKey OUTPUT, @jobLogKey = @jobLogKey OUTPUT;
	
    BEGIN TRY

		BEGIN TRANSACTION

		SET @initialJobLogKey =  @JobLogKey

		-- Log a point in the proceduere through this code 
		EXEC [internal].[p_InsertJobLog] @section = @section, @stepNo = @stepNo OUTPUT, @jobLogKey = @jobLogKey OUTPUT;
		        
        SET @section = 'Insert Vaildation Result'

		DECLARE @FileType int = (SELECT FIleTypeKey FROM internal.FileLog WHERE FileName = @Filename)
		DECLARE	@fileblobstorage nvarchar(200) = (SELECT [BlobFolderName]  FROM [internal].[FileType] WHERE FileTypeKey =@FileType)
		
		IF @FileType = 12 -- Kyofleet
		BEGIN
			UPDATE [internal].[FileLog]
			SET FilePath = @fileblobstorage + '/kyofleetclean/' + @Filename
			WHERE [FileName] = @filename
			ANd	FIleTypeKey = 12

		END

		
					
		COMMIT TRANSACTION

	END TRY

	BEGIN CATCH
		-- Test XACT_STATE for 0, 1, or -1.
		-- If 1, the transaction is committable.
		-- If -1, the transaction is uncommittable and should 
		--     be rolled back.
		-- XACT_STATE = 0 means there is no transaction and
		--     a commit or rollback operation would generate an error.

		IF (XACT_STATE()) = -1
			ROLLBACK TRANSACTION
 
		IF (XACT_STATE()) = 1
			COMMIT TRANSACTION

		-- log error
		--EXECUTE Internal.[p_InsertLogAndRaiseError] @section, @debug

		RETURN 1 -- return error as an exception has been generated!
	END CATCH
END