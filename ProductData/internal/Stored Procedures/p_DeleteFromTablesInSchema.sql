﻿CREATE PROCEDURE [internal].[p_DeleteFromTablesInSchema]
(
	@SchemaToDeleteFrom NVARCHAR(100)
,	@debug BIT = 0
)
AS
BEGIN
-- ==================================================================================================================
-- Author:			Ricoh
-- Create date:		06/08/2013
-- Description:		Deletes data from all tables in the specified schema
-- ------------------------------------------------------------------------------------------------------------------
-- Revision History:

-- ------------------------------------------------------------------------------------------------------------------
-- Debug Code:
/*
	[internal].p_DeleteFromTablesInSchema 'Internal'
*/
-- ==================================================================================================================

	SET NOCOUNT ON								-- Eliminate any dataset counts

    DECLARE @returnValue			INT = 0			-- Defaults to returning 0
	,		@JobLogKey				INT				-- Identity for Job Log Table
    ,		@stepNo					INT = 1			-- unique step counter 
    ,		@section				NVARCHAR(255)	-- document your steps by setting this variable
	,		@v_SchemaToDeleteFrom	NVARCHAR(100)
	,		@v_TableToDeleteFrom	NVARCHAR(200)
	,		@v_SQLtoExecute			NVARCHAR(MAX)
	,		@v_ID					INT
	,		@v_MaxID				INT

	-- Log the start time of the Procedure
    EXEC [internal].[p_InsertJobLog] @procId = @@PROCID, @JobLogKey = @JobLogKey OUTPUT
	
	BEGIN TRY

		SET @section = 'Loop through tables in schema and delete data'
		EXEC [internal].[p_InsertJobLog] @section = @section, @stepNo = @stepNo OUTPUT, @JobLogKey = @JobLogKey OUTPUT 
		
		SET @v_SchemaToDeleteFrom = @SchemaToDeleteFrom

		SELECT ROW_NUMBER() OVER (ORDER BY TABLE_NAME) AS ID, TABLE_NAME
		INTO #TablesToDeleteFrom
		FROM INFORMATION_SCHEMA.TABLES
		WHERE TABLE_SCHEMA = @v_SchemaToDeleteFrom
		AND TABLE_TYPE = 'BASE TABLE'

		SELECT @v_ID = MIN(ID) FROM #TablesToDeleteFrom
		SELECT @v_MaxID = Max(ID) FROM #TablesToDeleteFrom

		WHILE @v_ID <= @v_MaxID
		BEGIN
	
			SELECT @v_TableToDeleteFrom = TABLE_NAME FROM #TablesToDeleteFrom WHERE ID = @v_ID

			SET @v_SQLtoExecute =
				'
				DELETE FROM [' + @v_SchemaToDeleteFrom + '].[' + @v_TableToDeleteFrom + ']
				'
		
			EXEC sp_executesql @v_SQLtoExecute

			SET @v_ID = @v_ID + 1
		END

	END TRY

	BEGIN CATCH
	
		-- Log the error and raise it again
		EXECUTE [internal].[p_LogAndRaiseSQLError] @section = @section, @JobLogKey = @JobLogKey

		SET @returnValue = 1 -- Set different return value as an exception has been generated

    END CATCH

    -- Log the end of the Procedure Run, success or otherwise    
    EXEC [internal].[p_InsertJobLog] @JobLogKey = @JobLogKey
	
    RETURN @returnValue

END