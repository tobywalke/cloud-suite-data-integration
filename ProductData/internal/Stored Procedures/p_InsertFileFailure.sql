﻿CREATE PROCEDURE [internal].[p_InsertFileFailure]
AS

--==================================================================================================================
--Author:		Ridgian
--Created date: 13/11/2015
--Description:	Creates hardcoded value for file failure
-- ------------------------------------------------------------------------------------------------------------------
--Revision History:
--	Date			Person					Change

-- ------------------------------------------------------------------------------------------------------------------
--Debug Code:



--==================================================================================================================

BEGIN
    SET NOCOUNT ON								-- Eliminate any dataset counts

      DECLARE @returnValue		INT = 0			-- Defaults to returning 0
    ,		@initialJobLogKey	INT				-- Stores the initial job log key to use when an exception occcurs
	,		@jobKey			int				-- Identity for Job Table
	,		@JobLogKey			INT				-- Identity for Job Log Table
    ,		@stepNo				INT = 1			-- unique step counter 
    ,		@section			NVARCHAR(255)	-- document your steps by setting this variable

	
    BEGIN TRY

		BEGIN TRANSACTION
		SET @initialJobLogKey =  @JobLogKey

		TRUNCATE TABLE internal.[FileFailure]

		
		INSERT INTO	internal.[FileFailure]
		(			FileFailureKey
		,			FileFailureName
		)
		SELECT		
					-1
		,			'No Failure'
		UNION
		SELECT		
					1
		,			'File has incorrect number of columns'
		UNION
		SELECT		
					2
		,			'Unknown file name'
		UNION
		SELECT		
					3
		,			'File Meta Data Error'

		COMMIT TRANSACTION

    END TRY

    BEGIN CATCH


		SET @returnValue = 1 -- Set different return value as an exception has been generated

    END CATCH


    RETURN @returnValue

END