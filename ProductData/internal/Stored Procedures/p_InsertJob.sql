﻿CREATE PROCEDURE [internal].[p_InsertJob]
	@RunningJobKey bigint = NULL OUTPUT
AS
BEGIN

	/* ===============================================================================================================
	Author:			Carl Follows
	Create date:	20/10/2017
	Description:	Creates a new Job execution
	------------------------------------------------------------------------------------------------------------------
	Debug Code:
	

	=============================================================================================================== */

    SET NOCOUNT ON							-- Eliminate any dataset counts

    DECLARE @returnValue	int = 0			-- Defaults to returning 0
	,		@jobKey			bigint				-- Identity for Job Table
	,		@jobLogKey		bigint				-- Identity for Job Log Table
    ,		@stepNo			int = 1			-- unique step counter 
    ,		@section		nvarchar(2048)	-- document your steps by setting this variable

	SET @section = 'set up iknown job key if not available';

	SET IDENTITY_INSERT [internal].[Job] ON
			INSERT INTO [internal].[Job]
				(		[JobKey]
				,		[JobStatus]		
				,		[IsRunning]
				,		[StartDateTime]
			)	
			SELECT 
					-1			--JobKey
				,	'unknown'	--Job Status
				,	0
				,	GETDATE()				
			WHERE
					NOT EXISTS (SELECT * FROM [internal].[Job] WHERE [JobKey] = -1)
			
		SET IDENTITY_INSERT [internal].[Job] OFF	
	
    BEGIN TRY
	
		SET @section = 'Ensure there are no other active Job records';

			UPDATE	[internal].[Job]
			SET		[JobStatus] = 'Aborted'
			,		[IsRunning] = 0
			,		[FinishDateTime] = GETDATE()
			WHERE	[IsRunning] != 0;
		
		SET @section = 'Create a new job record';
	
			INSERT INTO [internal].[Job]
			(		[JobStatus]		
			,		[IsRunning]
			,		[StartDateTime]
			)
			SELECT	'Running'
			,		1
			,		GETDATE();
			
			-- Get the IDENTITY value 
			SET @RunningJobKey = SCOPE_IDENTITY();
		

    END TRY

    BEGIN CATCH

		-- Log error the error and raise it again
		EXECUTE [internal].[p_LogAndRaiseSQLError] @section = @section, @jobLogKey = @jobLogKey

		SET @returnValue = 1 -- Set different return value as an exception has been generated

    END CATCH

    RETURN @returnValue

END