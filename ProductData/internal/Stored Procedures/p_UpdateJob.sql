﻿CREATE PROCEDURE [internal].[p_UpdateJob]
	@JobStatus varchar(50)
AS
BEGIN

	/* ===============================================================================================================
	Author:			Carl Follows
	Create date:	20/10/2017
	Description:	Update the Job execution with progress
	------------------------------------------------------------------------------------------------------------------
	Debug Code:
	

	=============================================================================================================== */

    SET NOCOUNT ON							-- Eliminate any dataset counts

    DECLARE @returnValue	int = 0			-- Defaults to returning 0
	,		@jobKey			bigint				-- Identity for Job Table
	,		@jobLogKey		bigint				-- Identity for Job Log Table
    ,		@stepNo			int = 1			-- unique step counter 
    ,		@section		nvarchar(2048)	-- document your steps by setting this variable
	
    BEGIN TRY

		SET @section = 'Update the [internal].[Job] table';
		EXEC [internal].[p_InsertJobLog] @section = @section, @stepNo = @stepNo OUTPUT, @jobLogKey = @jobLogKey OUTPUT;

			UPDATE	[internal].[Job]
			SET		[JobStatus]									= @JobStatus
			,		[StageLoadCompleteDateTime]					= CASE WHEN @JobStatus = 'StageLoadComplete'				THEN GETDATE()	ELSE [StageLoadCompleteDateTime]				END
			,		[DataWarehouseLoadCompleteDateTime]			= CASE WHEN @JobStatus = 'DataWarehouseLoadComplete'		THEN GETDATE()	ELSE [DataWarehouseLoadCompleteDateTime]		END
			,		[DataModelProcessCompleteDateTime]	        = CASE WHEN @JobStatus = 'DataModelProcessCompleteDateTime'	THEN GETDATE()	ELSE [DataModelProcessCompleteDateTime]	        END
			,		[IsRunning]									= CASE WHEN @JobStatus IN ('Success', 'Failure')			THEN 0			ELSE [IsRunning]								END
			,		[FinishDateTime]							= CASE WHEN @JobStatus IN ('Success', 'Failure')			THEN GETDATE()	ELSE [FinishDateTime]							END
			WHERE	[IsRunning] = 1; --there should only ever be 1 record in this state
            
		SET @section = 'Update the [internal].[Job] table';
		EXEC [internal].[p_InsertJobLog] @section = @section, @stepNo = @stepNo OUTPUT, @jobLogKey = @jobLogKey OUTPUT;

            UPDATE	internal.Configuration
            SET		TruncateDataWarehouse = 'No'
            WHERE   @JobStatus = 'Success';

    END TRY

    BEGIN CATCH

		-- Log error the error and raise it again
		EXECUTE [internal].[p_LogAndRaiseSQLError] @section = @section, @jobLogKey = @jobLogKey

		SET @returnValue = 1 -- Set different return value as an exception has been generated

    END CATCH

    RETURN @returnValue

END