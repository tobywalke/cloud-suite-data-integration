﻿CREATE PROCEDURE [internal].[p_TruncateDataWarehouse]
AS

--==================================================================================================================
--Author:		Ridgian
--Created date: 13/11/2015
--Description:	Truncates all tables in the specified data warehouse database
-- ------------------------------------------------------------------------------------------------------------------
--Revision History:
--	Date			Person					Change

-- ------------------------------------------------------------------------------------------------------------------
--Debug Code:

--	EXEC [internal].[p_TruncateDataWarehouse]
--	SELECT * FROM internal.Error ORDER BY DateCreated DESC

--==================================================================================================================

BEGIN
    SET NOCOUNT ON								-- Eliminate any dataset counts

    DECLARE @returnValue		INT = 0			-- Defaults to returning 0
	,		@JobLogKey			INT				-- Identity for Job Log Table
    ,		@stepNo				INT = 1			-- unique step counter 
    ,		@section			NVARCHAR(255)	-- document your steps by setting this variable
	
	IF OBJECT_ID('tempdb..#Tables') IS NOT NULL 
		DROP TABLE #Tables;

	CREATE TABLE #Tables 		(SchemaName		SYSNAME	NOT NULL
								,TableName		SYSNAME	NOT NULL
								,TblObjectID	INT		NOT NULL);

	DECLARE @ForeignKeys TABLE	(FKName				SYSNAME		NOT NULL
								,FKObjID			INT			NOT NULL
								,FKIsDisabled		BIT			NOT NULL
								,FKIsNotForRepl		BIT			NOT NULL
								,FKDelRefAction		TINYINT		NOT NULL
								,FKUpdRefAction		TINYINT		NOT NULL
								,FKSchemaName		SYSNAME		NOT NULL
								,FKTableName		SYSNAME		NOT NULL
								,FKTableObjectID	SYSNAME		NOT NULL
								,FKColumnName		SYSNAME		NOT NULL
								,PKSchemaName		SYSNAME		NOT NULL
								,PKTableName		SYSNAME		NOT NULL
								,PKTableObjectID	SYSNAME		NOT NULL
								,PKColumnName		SYSNAME		NOT NULL);

	DECLARE	@curFKName			SYSNAME
	,		@curFKSchemaName	SYSNAME
	,		@curFKTableName		SYSNAME
	,		@curFKColumnName	SYSNAME
	,		@curCheckSnippet	NVARCHAR(20)
	,		@curPKSchemaName	SYSNAME
	,		@curPKTableName		SYSNAME
	,		@curPKColumnName	SYSNAME
	,		@SQLstatement		NVARCHAR(max);

	-- Abort the procedure if configured not to be run
	IF (SELECT TruncateDataWarehouse FROM internal.Configuration) = 'No'
		RETURN @returnValue

	-- Log the start time of the Procedure
    EXEC [internal].[p_InsertJobLog] @procId = @@PROCID, @JobLogKey = @JobLogKey OUTPUT
	
    BEGIN TRY

			--Get a list of tables for truncation
			SELECT @SQLstatement = '
									SELECT		ss.name			AS SchemaName
									,			st.name			AS TableName
									,			st.[object_id]	AS TblObjectID
									FROM		sys.tables st
									INNER JOIN	sys.schemas ss
									ON			st.[schema_id] = ss.[schema_id]
									WHERE		st.[type] = ''U''
									AND			(	ss.name =	 ''dim''
												OR	ss.name LIKE ''fact''
												)';

			INSERT INTO #Tables
			(			SchemaName
			,			TableName
			,			TblObjectID)
			EXEC sp_executesql @stmt = @SQLstatement;

			--Get a list of the foreign keys on these tables
			SELECT @SQLstatement = '
									WITH sysTableCols AS	(SELECT		ss.name			AS SchemaName
															,			st.name			AS TableName
															,			st.[object_id]	AS TableObjectID
															,			sc.name			AS ColumnName
															,			sc.column_id	AS ColumnID
															FROM		sys.schemas ss
															INNER JOIN	sys.tables st
															ON			ss.[schema_id] = st.[schema_id]
															INNER JOIN	sys.columns sc
															ON			st.[object_id] = sc.[object_id] 
															WHERE		st.[object_id] IN	(SELECT TblObjectID
																							FROM	#Tables))
																							
									SELECT		sfk.name						AS FKName
									,			sfk.[object_id]					AS FKObjectID
									,			sfk.is_disabled					AS FKIsDisabled
									,			sfk.is_not_for_replication		AS FKIsNotForRepl
									,			sfk.delete_referential_action	AS FKDelRefAction
									,			sfk.update_referential_action	AS FKUpdRefAction
									,			stcPnt.SchemaName				AS FKSchemaName
									,			stcPnt.TableName				AS FKTableName
									,			stcPnt.TableObjectID			AS FKTableObjectID
									,			stcPnt.ColumnName				AS FKColumnName
									,			stcRef.SchemaName				AS PKSchemaName
									,			stcRef.TableName				AS PKTableName
									,			stcRef.TableObjectID			AS PKTableObjectID
									,			stcRef.ColumnName				AS PKColumnName
									FROM		sys.foreign_keys sfk
									INNER JOIN	sys.foreign_key_columns sfkc 
									ON			sfk.[object_id] = sfkc.constraint_object_id
									INNER JOIN	sysTableCols stcPnt
									ON			sfk.parent_object_id = stcPnt.TableObjectID
									AND			sfkc.parent_column_id = stcPnt.ColumnID
									INNER JOIN	sysTableCols stcRef
									ON			sfk.referenced_object_id = stcRef.TableObjectID
									AND			sfkc.referenced_column_id = stcRef.ColumnID';

			INSERT INTO	@ForeignKeys
			(			FKName
			,			FKObjID
			,			FKIsDisabled
			,			FKIsNotForRepl
			,			FKDelRefAction
			,			FKUpdRefAction
			,			FKSchemaName
			,			FKTableName
			,			FKTableObjectID
			,			FKColumnName
			,			PKSchemaName
			,			PKTableName
			,			PKTableObjectID
			,			PKColumnName)

			EXEC sp_executesql @stmt = @SQLstatement;

		SET @section = 'Cursor through the foreign keys dropping them';
		EXEC [internal].[p_InsertJobLog] @section = @section, @stepNo = @stepNo OUTPUT, @JobLogKey = @JobLogKey OUTPUT
        
			DECLARE curForeignKeys CURSOR FOR
				SELECT		FKName
				,			FKSchemaName
				,			FKTableName
				,			FKColumnName
				,			CASE WHEN FKIsDisabled = 1 THEN 'NOCHECK' ELSE 'CHECK' END AS CheckSnippet
				,			PKSchemaName
				,			PKTableName
				,			PKColumnName
				FROM		@ForeignKeys
				ORDER BY	FKName

			OPEN curForeignKeys
			FETCH NEXT FROM curForeignKeys INTO @curFKName, @curFKSchemaName, @curFKTableName, @curFKColumnName, @curCheckSnippet, @curPKSchemaName, @curPKTableName, @curPKColumnName

			WHILE @@FETCH_STATUS = 0
			BEGIN
 
				SET @SQLstatement = 'ALTER TABLE [' + @curFKSchemaName + '].[' + @curFKTableName + ']'
									+ ' DROP CONSTRAINT [' + @curFKName + '];';
				EXEC sp_executesql @stmt = @SQLstatement;
				PRINT @SQLstatement
   
				FETCH NEXT FROM curForeignKeys INTO @curFKName, @curFKSchemaName, @curFKTableName, @curFKColumnName, @curCheckSnippet, @curPKSchemaName, @curPKTableName, @curPKColumnName
			END

			CLOSE curForeignKeys

		SET @section = 'Cursor through the tables truncating them';
		EXEC [internal].[p_InsertJobLog] @section = @section, @stepNo = @stepNo OUTPUT, @JobLogKey = @JobLogKey OUTPUT
        
			DECLARE curTables CURSOR FOR
				SELECT		SchemaName
				,			TableName
				FROM		#Tables
				ORDER BY	TableName

			OPEN curTables
			FETCH NEXT FROM curTables INTO @curFKSchemaName, @curFKTableName

			WHILE @@FETCH_STATUS = 0
			BEGIN
 
				SET @SQLstatement = 'TRUNCATE TABLE [' + @curFKSchemaName + '].[' + @curFKTableName + '];';
				EXEC sp_executesql @stmt = @SQLstatement;
				PRINT @SQLstatement
   
				FETCH NEXT FROM curTables INTO @curFKSchemaName, @curFKTableName
			END

			CLOSE curTables
			DEALLOCATE curTables

		SET @section = 'Manual truncation of tables not isolated by Schema';
		EXEC [internal].[p_InsertJobLog] @section = @section, @stepNo = @stepNo OUTPUT, @JobLogKey = @JobLogKey OUTPUT
			
			TRUNCATE TABLE [internal].[JobLog]
			delete from  [internal].[Error]
			
 
		SET @section = 'Cursor through the foreign keys recreating them';
		EXEC [internal].[p_InsertJobLog] @section = @section, @stepNo = @stepNo OUTPUT, @JobLogKey = @JobLogKey OUTPUT
        
			OPEN curForeignKeys
			FETCH NEXT FROM curForeignKeys INTO @curFKName, @curFKSchemaName, @curFKTableName, @curFKColumnName, @curCheckSnippet, @curPKSchemaName, @curPKTableName, @curPKColumnName

			WHILE @@FETCH_STATUS = 0
			BEGIN
 
				SET @SQLstatement = 'ALTER TABLE [' + @curFKSchemaName + '].[' + @curFKTableName + ']'
									+ ' WITH ' + @curCheckSnippet 
									+ ' ADD CONSTRAINT [' + @curFKName + ']'
									+ ' FOREIGN KEY ([' + @curFKColumnName + '])'
									+ ' REFERENCES [' + @curPKSchemaName + '].[' + @curPKTableName + '] ([' + @curPKColumnName + ']);';
				EXEC sp_executesql @stmt = @SQLstatement;
				PRINT @SQLstatement
   
				FETCH NEXT FROM curForeignKeys INTO @curFKName, @curFKSchemaName, @curFKTableName, @curFKColumnName, @curCheckSnippet, @curPKSchemaName, @curPKTableName, @curPKColumnName
			END

			CLOSE curForeignKeys
			DEALLOCATE curForeignKeys

    END TRY

    BEGIN CATCH

		-- Log error the error and raise it again
		EXECUTE [internal].[p_LogAndRaiseSQLError] @section = @section, @JobLogKey = @JobLogKey

		SET @returnValue = 1 -- Set different return value as an exception has been generated
		
		IF (CURSOR_STATUS('global', 'curForeignKeys') = -1) 
		BEGIN
			DEALLOCATE curForeignKeys;
		END
		IF (CURSOR_STATUS('global', 'curForeignKeys') = 1) 
		BEGIN
			CLOSE curForeignKeys;
			DEALLOCATE curForeignKeys;
		END
		
		IF (CURSOR_STATUS('global', 'curTables') = -1) 
		BEGIN
			DEALLOCATE curTables;
		END
		IF (CURSOR_STATUS('global', 'curTables') = 1) 
		BEGIN
			CLOSE curTables;
			DEALLOCATE curTables;
		END

    END CATCH

    -- Log the end of the Procedure Run, success or otherwise    
    EXEC [internal].[p_InsertJobLog] @JobLogKey = @JobLogKey

    RETURN @returnValue

END