﻿CREATE PROCEDURE [internal].[p_InsertDemoPostcodes]
AS

--==================================================================================================================
--Author:		Ricoh
--Created date: 26/06/2019
--Description:	Creates client demo postcodes
-- ------------------------------------------------------------------------------------------------------------------
--Revision History:
--	Date			Person					Change

-- ------------------------------------------------------------------------------------------------------------------
--Debug Code:



--==================================================================================================================

BEGIN
    SET NOCOUNT ON								-- Eliminate any dataset counts

    DECLARE @returnValue		INT = 0			-- Defaults to returning 0
	,		@jobKey				INT				-- Identity for Job Table
	,		@jobLogKey			INT				-- Identity for Job Log Table
    ,		@stepNo				INT = 1			-- unique step counter 
    ,		@section			NVARCHAR(255)	-- document your steps by setting this variable

    BEGIN TRY
		 
		SET @section = 'Insert client demo record'
		
		TRUNCATE TABLE etl.DemoPostcodes		
		-- Postcodes for British sports stadium
		INSERT INTO etl.[DemoPostcodes] SELECT 'NW1 2SA'
		INSERT INTO etl.[DemoPostcodes] SELECT 'S71 1ET'
		INSERT INTO etl.[DemoPostcodes] SELECT 'B6 6HE'
		INSERT INTO etl.[DemoPostcodes] SELECT 'BB2 4JF'
		INSERT INTO etl.[DemoPostcodes] SELECT 'FY1 6JJ'
		INSERT INTO etl.[DemoPostcodes] SELECT 'BL6 6JW'
		INSERT INTO etl.[DemoPostcodes] SELECT 'BH7 7AF'
		INSERT INTO etl.[DemoPostcodes] SELECT 'BN1 9BL'
		INSERT INTO etl.[DemoPostcodes] SELECT 'BB10 4BX'
		INSERT INTO etl.[DemoPostcodes] SELECT 'CF11 8AZ'
		INSERT INTO etl.[DemoPostcodes] SELECT 'SW6 1HS'
		INSERT INTO etl.[DemoPostcodes] SELECT 'SE25 6PU'
		INSERT INTO etl.[DemoPostcodes] SELECT 'DE24 8XL'
		INSERT INTO etl.[DemoPostcodes] SELECT 'L4 4EL'
		INSERT INTO etl.[DemoPostcodes] SELECT 'HD1 6PX'
		INSERT INTO etl.[DemoPostcodes] SELECT 'LS11 0ES'
		INSERT INTO etl.[DemoPostcodes] SELECT 'LE2 7FL'
		INSERT INTO etl.[DemoPostcodes] SELECT 'M11 3FF'
		INSERT INTO etl.[DemoPostcodes] SELECT 'M16 0RA'
		INSERT INTO etl.[DemoPostcodes] SELECT 'TS3 6RS'
		INSERT INTO etl.[DemoPostcodes] SELECT 'NE1 4ST'
		INSERT INTO etl.[DemoPostcodes] SELECT 'NR1 1JE'
		INSERT INTO etl.[DemoPostcodes] SELECT 'PO4 8RA'
		INSERT INTO etl.[DemoPostcodes] SELECT 'S2 4SU'
		INSERT INTO etl.[DemoPostcodes] SELECT 'S6 1SW'
		INSERT INTO etl.[DemoPostcodes] SELECT 'SO14 5FP'
		INSERT INTO etl.[DemoPostcodes] SELECT 'SA1 2FA'
		INSERT INTO etl.[DemoPostcodes] SELECT 'N17 0BX'
		INSERT INTO etl.[DemoPostcodes] SELECT 'WD18 0ER'
		INSERT INTO etl.[DemoPostcodes] SELECT 'B71 4LF'
		INSERT INTO etl.[DemoPostcodes] SELECT 'WV1 4QR'
		INSERT INTO etl.[DemoPostcodes] SELECT 'BS3 2EJ'
		INSERT INTO etl.[DemoPostcodes] SELECT 'PL2 3DQ'
		INSERT INTO etl.[DemoPostcodes] SELECT 'SY11 4AS'
		INSERT INTO etl.[DemoPostcodes] SELECT 'LL11 2AH'
		INSERT INTO etl.[DemoPostcodes] SELECT 'G51 2XD'
		INSERT INTO etl.[DemoPostcodes] SELECT 'G40 3RE'
		INSERT INTO etl.[DemoPostcodes] SELECT 'EH7 5QG'
		INSERT INTO etl.[DemoPostcodes] SELECT 'EH11 2NL'
		INSERT INTO etl.[DemoPostcodes] SELECT 'AB24 5QH'
		INSERT INTO etl.[DemoPostcodes] SELECT 'KY1 1RZ'
		INSERT INTO etl.[DemoPostcodes] SELECT 'B9 4RL'
		INSERT INTO etl.[DemoPostcodes] SELECT 'BD8 7DY'
		INSERT INTO etl.[DemoPostcodes] SELECT 'OL1 2PAA'
		INSERT INTO etl.[DemoPostcodes] SELECT 'IP1 2DA'
		INSERT INTO etl.[DemoPostcodes] SELECT 'HU3 6HU'
		INSERT INTO etl.[DemoPostcodes] SELECT 'SR5 1SU'
		INSERT INTO etl.[DemoPostcodes] SELECT 'RG2 0FL'
		INSERT INTO etl.[DemoPostcodes] SELECT 'NG2 5FJ'
		INSERT INTO etl.[DemoPostcodes] SELECT 'WN5 0UH'
		INSERT INTO etl.[DemoPostcodes] SELECT 'CF10 1NS'
		INSERT INTO etl.[DemoPostcodes] SELECT 'TW2 7BA'
		INSERT INTO etl.[DemoPostcodes] SELECT 'Dublin 4'
		INSERT INTO etl.[DemoPostcodes] SELECT 'Dublin 3'
		INSERT INTO etl.[DemoPostcodes] SELECT 'BT12 6LW'
		INSERT INTO etl.[DemoPostcodes] SELECT 'SS2 6NQ'

    END TRY

    BEGIN CATCH

		-- Log the error and raise it again
		EXECUTE [internal].[p_LogAndRaiseSQLError] @section = @section, @jobLogKey = @jobLogKey

		SET @returnValue = 1 -- Set different return value as an exception has been generated

    END CATCH


    RETURN @returnValue

END