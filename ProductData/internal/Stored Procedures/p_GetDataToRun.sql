﻿CREATE PROCEDURE [internal].[p_GetDataToRun]
AS
BEGIN

	/* ===============================================================================================================
	Author:			Ridgian
	Create date:	09/04/2018
	Description:	Creates a new Job execution
	------------------------------------------------------------------------------------------------------------------
	Amendments:
	02/05/2018 - Dave Morrow - Added sanity check after ETL issue caused flag to stay on and ended all processing for days.
	14/05/2018 - Dave Morrow - Fix so sanity check filters on main data warehouse proc.
	
	Debug Code:
	

	=============================================================================================================== */

    SET NOCOUNT ON							-- Eliminate any dataset counts

    DECLARE @returnValue	int = 0			-- Defaults to returning 0
	,		@jobKey			int				-- Identity for Job Table
	,		@jobLogKey		int				-- Identity for Job Log Table
    ,		@stepNo			int = 1			-- unique step counter 
    ,		@section		nvarchar(2048)	-- document your steps by setting this variable
	,		@filesToRun		int = 0
	,		@ETLInProgress  char(3) = 'No'
	,		@LatestJobDate	DATETIME


    BEGIN TRY

		SET @section = 'Tidy up the job log';

		-- control table check not required for Pritn Insights as all handled by API.


		--IF @ETLINprogress = 'No'
		--BEGIN
		
			SET @filesToRun = (	SELECT	COUNT(*)
								FROM		internal.FileLog fl
								INNER JOIN	internal.FileStatus fs
								ON			fl.FileStatusKey = fs.FileStatusKey
								WHERE		fs.FileStatusName = 'File Ready for Processing')
		--END

    END TRY

    BEGIN CATCH

		-- Log error the error and raise it again
		EXECUTE [internal].[p_LogAndRaiseSQLError] @section = @section, @jobLogKey = @jobLogKey

		SET @filesToRun = 0 -- Set different return value as an exception has been generated

    END CATCH

    -- Log the end of the Procedure Run, success or otherwise    
    --EXEC [internal].[p_InsertJobLog] @jobLogKey = @jobLogKey

    SELECT @filesToRun AS FileCount

END