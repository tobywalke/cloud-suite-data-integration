﻿CREATE PROCEDURE [internal].[p_LogAndRaiseSQLError]
(
    @section    VARCHAR(255)
,	@jobLogKey	INT
)
AS

--==================================================================================================================
--Author:		Ricoh		
--Created date: 03/11/2015
--Description:	Stored procedure to log and raise SQL errors
-- ------------------------------------------------------------------------------------------------------------------
--Revision History:
--	Date			Person					Change

-- ------------------------------------------------------------------------------------------------------------------
--Debug Code:

--	DECLARE @RC int
--	DECLARE @section varchar(255)
--	DECLARE @jobLogKey int

---- TODO: Set parameter values here.

--	EXECUTE @RC = [internal].[UpdateJobLog] 
--	@section
--  ,@jobLogKey
--	GO
--==================================================================================================================

BEGIN
	DECLARE @errorMessage   NVARCHAR(4000)
    ,		@errorSeverity	INT
    ,		@errorKey		INT

    SET @errorMessage = ERROR_MESSAGE()
    SET @errorSeverity = ERROR_SEVERITY()

    INSERT INTO	[internal].[Error]
    (
				[ErrorSource]
    ,			[ErrorNumber]
    ,			[ErrorMessage]
    ,			[ErrorState]
    ,			[ErrorSeverity]
    ,			[Routine]
    ,			[LineNo]
    ,			[DatabaseName]
    ,			[ApplicationName]
    ,			[UserName]
    ,			[HostName]
    ,			[DateCreated]
    )
    VALUES
    (
				'SQL'
    ,			ERROR_NUMBER()
    ,			@errorMessage
    ,			ERROR_STATE()
    ,			@errorSeverity
    ,			ERROR_PROCEDURE()
    ,			ERROR_LINE()
    ,			DB_NAME()
    ,			APP_NAME()
    ,			SYSTEM_USER
    ,			HOST_NAME()
    ,			GETDATE()
    )
    SET @errorKey = SCOPE_IDENTITY()

    --UPDATE		[internal].[JobLog]
    --SET			[EndTime] = GETDATE()
    --,			[Section] = @section
    --,			[ErrorKey] = @errorKey
    --WHERE		[JobLogKey] = @jobLogKey

    RAISERROR ( '%s',@errorSeverity,1,@errorMessage)

    RETURN 0
END