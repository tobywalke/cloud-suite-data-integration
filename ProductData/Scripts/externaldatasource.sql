﻿CREATE EXTERNAL DATA SOURCE [prstortst01]
WITH (
    TYPE = BLOB_STORAGE,
    LOCATION = N'https://prstortst01.blob.core.windows.net',
    CREDENTIAL = [ProductdataBlobStorageCred]
)