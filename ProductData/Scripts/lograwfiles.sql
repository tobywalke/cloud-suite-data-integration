﻿WITH cte
AS
(
SELECT 
	[FilePath]
,	dc.Container
,	dg.DataGroup
,	dt.DataType
,	fl.[Filename]
FROM [internal].[FileLogRaw]
CROSS APPLY
( SELECT SUBSTRING([FilePath],1,charindex('/',[FilePath]))	AS Container) dc
CROSS APPLY
( SELECT SUBSTRING([FilePath],charindex('/',[FilePath])+1,charindex('/',SUBSTRING([FilePath],charindex('/',[FilePath])+1,200))) AS DataGroup) dg
CROSS APPLY
( SELECT SUBSTRING([FilePath],(LEN(dc.Container)+ LEN(dg.DataGroup))+1,CHARINDEX('/' ,SUBSTRING([FilePath],(LEN(dc.Container)+ LEN(dg.DataGroup))+1,200))) AS DataType) dt 
CROSS APPLY
( SELECT REVERSE(SUBSTRING(REVERSE([FilePath]),1,charindex('/',REVERSE([FilePath]))-1))	AS [Filename]) fl
)

INSERT INTO [internal].[FileLog]
           ([JobKey]
           ,[FilePath]
           ,[FileName]
           ,[OriginalFileName]
           ,[FileLoadDate]
           ,[FIleTypeKey]
           ,[FileStatusKey]
           ,[EmailStatusKey])



SELECT	-1 as JobKey
,		cte.FilePath
,		cte.[Filename]
,		cte.[Filename]
,		getdate()
,		ft.FileTypeKey
,		1 AS EmailStatus
,		1 AS FileStatus
FROM	internal.DataGroup dg
INNER JOIN
		internal.DataType dt
ON		dg.DataGroupKey = dt.DataGroupKey
INNER JOIN
		internal.FileType ft
ON		dt.DataTypeKey = ft.DataTypeKey
INNER  JOIN
		cte
ON		('raw/'+dg.DataGroupName+'/'+dt.DataTypeName+'/') = (cte.Container+cte.DataGroup+cte.DataType) 

select * from internal.FileLog
