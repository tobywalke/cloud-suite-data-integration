﻿create PROCEDURE [etl].[p_InsertDimCalendar]
AS

--==================================================================================================================
--Author:		Ricoh
--Created date: 10/11/2015
--Description:	Inserts records into the Calendar dimension for the specified time period.
--				NOTE: The template version of this procedure will require additions and/or modification based
--				on the individual requirements of a particular project.
--				For example:
--				- Use of a financial calendar year.
--				- use of specialised period calendars, such as 445.
--				The developer should also remove any date attributes that are not relevant to
--				the particular project - less is more!
-- ------------------------------------------------------------------------------------------------------------------
--Revision History:
--	Date			Person					Change
--	10/02/2016		James Coulter			Replaced WHILE LOOP with Numbers table
--	22/02/2016		Dave Morrow				Reviewed and amended table structure.
--	24/11/2016		Dave Morrow				Implemented CTEs and ISO Week.
-- ------------------------------------------------------------------------------------------------------------------
--Debug Code:
--  DELETE FROM dim.Calendar
--	EXEC [etl_dw].[p_InsertDimCalendar]
--	SELECT * FROM dim.Calendar
--	SELECT * FROM internal.Error ORDER BY DateCreated DESC

--==================================================================================================================

BEGIN
    SET NOCOUNT ON								-- Eliminate any dataset counts

    DECLARE @returnValue		INT = 0			-- Defaults to returning 0
	,		@JobLogKey			INT				-- Identity for Job Log Table
    ,		@stepNo				INT = 1			-- unique step counter 
    ,		@section			NVARCHAR(255)	-- document your steps by setting this variable

	
    BEGIN TRY

		SET @section = 'Insert calendar records'

		-- Set dateformat for correct string interpretation
        SET DATEFORMAT DMY;
            
        -- Set which day is the first day of week (1=Mon, 2=Tue, 3=Wed, 4=Thu, 5=Fri, 6=Sat, 7=Sun)
        SET DATEFIRST 1;

		-- Declare variables
        DECLARE @FirstDate              DATETIME
        DECLARE @LastDate               DATETIME
		DECLARE @CalendarDate			DATETIME
		DECLARE @CurrentReportingDate	DATETIME
		DECLARE @UnspecifiedDate		DATETIME
		
		-- The starting date for adding new records to
        -- the calendar dimension is the day after the max date
        SET @FirstDate = (SELECT CalendarStartDate FROM internal.[Configuration]);

		-- Default to date specified in the configuration table.
        IF @FirstDate IS NULL SET @FirstDate = (SELECT DATEADD(D, 1, MAX(CalendarDate)) FROM dim.Calendar);

		-- Default to first date of current year.
		IF @FirstDate IS NULL SET @FirstDate = DATEADD(YEAR, -1, (SELECT CAST('01/01/'+ DATENAME(YYYY, GETDATE()) AS DATE)));

		-- Set the max date to insert as the end of the current year.
		-- This will require modification where the system is based on a financial/reporting year.
		SET @LastDate =	(SELECT CAST('31/12/'+ DATENAME(YYYY, GETDATE()) AS DATE));

		-- Set starting point to the start of the first year.
		-- This will require modification where the system is based on a financial/reporting year.
        SET @CalendarDate = CAST('01/01/' + DATENAME(YYYY, @FirstDate) AS DATE);

		-- Assume current reporting date is yesterday. May require modification for individual projects.
		-- For example, If processing is weekly rather than nightly, the requirement may be different.
		-- This is used to set the current status (Past/Current/Future) of all the time period attributes
		SET @CurrentReportingDate = DATEADD(dd, DATEDIFF(dd, 1, getdate()), 0)

		-- Use date long in past for unspecified member. This may require modification depending on the needs of the project.
		SET @UnspecifiedDate = CAST('01/01/1900' AS DATETIME) 

		-- Number table is used to provide a set based approach to generating a sequence of dates across a range.
		-- This is more efficient than the old cursor method.
		-- The above will cater for up to 100,000 dates. In the unlikely event of a greater range being required, it
		-- just needs another row adding to the number table.
		; WITH Numbers (N) 
				AS  -- Use number table to generate a date range.
				(
					SELECT		ROW_NUMBER() OVER (ORDER BY n1)-1 AS N
					FROM		(VALUES(1),(1),(1),(1),(1),(1),(1),(1),(1),(1)) t1 (n1) --10
					CROSS JOIN	(VALUES(1),(1),(1),(1),(1),(1),(1),(1),(1),(1)) t2 (n2) --100
					CROSS JOIN	(VALUES(1),(1),(1),(1),(1),(1),(1),(1),(1),(1)) t3 (n3) --1000
					CROSS JOIN	(VALUES(1),(1),(1),(1),(1),(1),(1),(1),(1),(1)) t4 (n4) --10000
					CROSS JOIN	(VALUES(1),(1),(1),(1),(1),(1),(1),(1),(1),(1)) t5 (n5) --100000
				)
		,	DateList 
			AS
			(

			-- Unspecified date - best to hide this unless it is REALLY needed. Otherwise it is confusing for the user.
			-- Consider defaulting unspecified dates to the first date in the calendar, for example.
			SELECT @UnspecifiedDate AS CalendarDate
								
			UNION ALL
								
			SELECT	DATEADD(DAY, N, @CalendarDate) AS CalendarDate
			FROM	Numbers
			WHERE	DATEADD(DAY, N, @CalendarDate) <= @LastDate
			)
			,	Calendar1
				AS
				(
					SELECT
							CalendarDate													AS CalendarDate
						,	DATEPART(DAY, CalendarDate)										AS DayNumber
						,	DATEPART(ISO_WEEK, CalendarDate)								AS WeekNumber
						,	DATEPART(MONTH, CalendarDate)									AS MonthNumber
						,	LEFT(DATENAME(MONTH, CalendarDate), 3)							AS MonthShortName
						,	DATEPART(QUARTER, CalendarDate)									AS QuarterNumber
						,	CAST(DATEPART(QUARTER, CalendarDate) AS NCHAR(1))				AS QuarterShortName
						,	DATEPART(YEAR, CalendarDate)									AS YearNumber
						,	RIGHT(CAST(DATEPART(YEAR, CalendarDate) AS NVARCHAR(4)),2)		AS YearShortName
						,	CASE
								WHEN @CurrentReportingDate > CalendarDate THEN 1
								WHEN @CurrentReportingDate = CalendarDate THEN 2
								ELSE 3
							END																AS CurrentDateStatusKey
						,   CASE
								WHEN YEAR(@CurrentReportingDate) > YEAR(CalendarDate) 
									OR (YEAR(@CurrentReportingDate) = YEAR(CalendarDate) AND DATEPART(MONTH, @CurrentReportingDate) > DATEPART(MONTH, CalendarDate)) THEN 1                    
								WHEN DATEPART(MONTH, @CurrentReportingDate) = DATEPART(MONTH, CalendarDate)
									AND YEAR(@CurrentReportingDate) =  YEAR(CalendarDate) THEN 2
								ELSE 3
							END																AS CurrentMonthStatusKey

						,	CASE
								WHEN DATEPART(QUARTER, @CurrentReportingDate) > DATEPART(QUARTER, CalendarDate) 
									OR YEAR(@CurrentReportingDate) >  YEAR(CalendarDate) THEN 1
								WHEN DATEPART(QUARTER, @CurrentReportingDate) = DATEPART(QUARTER, CalendarDate)
									AND YEAR(@CurrentReportingDate) =  YEAR(CalendarDate) THEN 2
								ELSE 3
							END																AS CurrentQuarterStatusKey
						,	CASE
								WHEN YEAR(@CurrentReportingDate) >  YEAR(CalendarDate) THEN 1
								WHEN YEAR(@CurrentReportingDate) =  YEAR(CalendarDate) THEN 2
								ELSE 3
							END																AS CurrentYearStatusKey
						
						FROM DateList
					)
					,	Calendar2
						AS
						(
							SELECT
								CalendarDate
							,	DayNumber
							,	MonthNumber
							,	MonthShortName
							,	QuarterNumber
							,	QuarterShortName
							,	YearNumber
							,	YearShortName
							,	CurrentDateStatusKey
							-- Status to allow easy selection of current, previous and future
							-- date periods as a group.
							-- These are retrospectively updated as time moves forward.
							,	CASE CurrentDateStatusKey
									WHEN 1 THEN 'Past Date'
									WHEN 2 THEN 'Current Date'
									ELSE 'Future Date'
								END AS CurrentDateStatusName
							,	CurrentMonthStatusKey
							,	CASE CurrentMonthStatusKey
									WHEN 1 THEN 'Past Month'
									WHEN 2 THEN 'Current Month'
									ELSE 'Future Month'
								END AS CurrentMonthStatusName
							,	CurrentQuarterStatusKey
							,	CASE CurrentQuarterStatusKey
									WHEN 1 THEN 'Past Quarter'
									WHEN 2 THEN 'Current Quarter'
									ELSE 'Future Quarter'
								END AS CurrentQuarterStatusName
							,	CurrentYearStatusKey
							,	CASE CurrentYearStatusKey
									WHEN 1 THEN 'Past Year'
									WHEN 2 THEN 'Current Year'
									ELSE 'Future Year'
								END AS CurrentYearStatusName
							FROM	Calendar1	
						)
						,	cte_Load
							AS
							(
								SELECT
									CASE
										WHEN CalendarDate = @UnspecifiedDate
										THEN CAST(CONVERT(NVARCHAR(8),@UnspecifiedDate,112) AS INT) --19000101
										ELSE CAST(CONVERT(NVARCHAR(8),CalendarDate,112) AS INT) 
									END AS CalendarKey
								,	CalendarDate
								,	DayNumber
								,	DATEPART(DW, CalendarDate) AS WeekDayKey
								,	LEFT(DATENAME(WEEKDAY, CalendarDate),3) AS WeekDayName
								,	CASE
										WHEN DayNumber < 6 THEN 1
										ELSE 2
									END AS WeekendStatusKey

								,	CASE
										WHEN DayNumber < 6 THEN 'Week Day'
										ELSE 'Weekend'
									END AS WeekendStatusName
								,	YearNumber * 100 + MonthNumber AS MonthYearKey
								,	MonthShortName + '-' + YearShortName AS MonthYearName
								,	MonthNumber
								,	MonthShortName
								,	YearNumber * 10 + QuarterNumber AS QuarterYearKey
								,	'Q' + QuarterShortName + '-' + YearShortName AS QuarterYearName
								,	YearNumber
								,	CurrentDateStatusKey
								,	CurrentDateStatusName
								,	CurrentMonthStatusKey
								,	CurrentMonthStatusName
								,	CurrentQuarterStatusKey
								,	CurrentQuarterStatusName
								,	CurrentYearStatusKey
								,	CurrentYearStatusName
								FROM Calendar2
							)

			MERGE		
						[dim].[Calendar] WITH(TABLOCK) AS TARGET
			USING		
						cte_load
			ON			
						TARGET.CalendarKey = cte_load.CalendarKey
			WHEN MATCHED THEN
			UPDATE
			SET
						CalendarDate = cte_load.CalendarDate
			,			DayNumber = cte_load.DayNumber
			,			WeekDayKey = cte_load.WeekDayKey
			,			WeekDayName = cte_load.WeekDayName
			,			WeekendStatusKey = cte_load.WeekendStatusKey
			,			WeekendStatusName = cte_load.WeekendStatusName
			,			MonthYearKey = cte_load.MonthYearKey
			,			MonthYearName = cte_load.MonthYearName
			,			MonthNumber = cte_load.MonthNumber
			,			MonthShortName = cte_load.MonthShortName
			,			QuarterYearKey = cte_load.QuarterYearKey
			,			QuarterYearName = cte_load.QuarterYearName
			,			YearNumber = cte_load.YearNumber
			,			CurrentDateStatusKey = cte_load.CurrentDateStatusKey
			,			CurrentDateStatusName = cte_load.CurrentDateStatusName
			,			CurrentMonthStatusKey = cte_load.CurrentMonthStatusKey
			,			CurrentMonthStatusName = cte_load.CurrentMonthStatusName
			,			CurrentQuarterStatusKey = cte_load.CurrentQuarterStatusKey
			,			CurrentQuarterStatusName = cte_load.CurrentQuarterStatusName
			,			CurrentYearStatusKey = cte_load.CurrentYearStatusKey
			,			CurrentYearStatusName = cte_load.CurrentYearStatusName
			,			UpdatedDate = GetDate()
	
			WHEN NOT MATCHED BY TARGET THEN
			INSERT
			(
						Calendarkey
			,			CalendarDate 
			,			DayNumber 
			,			WeekDayKey 
			,			WeekDayName 
			,			WeekendStatusKey
			,			WeekendStatusName 
			,			MonthYearKey
			,			MonthYearName
			,			MonthNumber
			,			MonthShortName
			,			QuarterYearKey
			,			QuarterYearName
			,			YearNumber
			,			CurrentDateStatusKey
			,			CurrentDateStatusName
			,			CurrentMonthStatusKey
			,			CurrentMonthStatusName
			,			CurrentQuarterStatusKey
			,			CurrentQuarterStatusName
			,			CurrentYearStatusKey
			,			CurrentYearStatusName
			,			CreatedDate
			)
			VALUES
			(
						CalendarKey
			,			CalendarDate 
			,			DayNumber 
			,			WeekDayKey 
			,			WeekDayName 
			,			WeekendStatusKey
			,			WeekendStatusName 
			,			MonthYearKey
			,			MonthYearName
			,			MonthNumber
			,			MonthShortName
			,			QuarterYearKey
			,			QuarterYearName
			,			YearNumber
			,			CurrentDateStatusKey
			,			CurrentDateStatusName
			,			CurrentMonthStatusKey
			,			CurrentMonthStatusName
			,			CurrentQuarterStatusKey
			,			CurrentQuarterStatusName
			,			CurrentYearStatusKey
			,			CurrentYearStatusName
			,			GETDATE()
			)
	;

    END TRY

    BEGIN CATCH

		-- Log the error and raise it again
		EXECUTE [internal].[p_LogAndRaiseSQLError] @section = @section, @JobLogKey = @JobLogKey

		SET @returnValue = 1 -- Set different return value as an exception has been generated

    END CATCH

    RETURN @returnValue

END