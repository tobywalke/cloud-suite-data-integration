﻿
CREATE PROCEDURE [etl].[p_InsertDimTime]
AS

--==================================================================================================================
--Author:		Ridgian
--Created date: 10/11/2015
--Description:	Inserts records into the Time dimension at minute granularity
-- ------------------------------------------------------------------------------------------------------------------
--Revision History:
--	Date			Person					Change
--	10/02/2016		James Coulter			Replaced WHILE LOOP with Numbers table
-- ------------------------------------------------------------------------------------------------------------------
--Debug Code:

--	EXEC [etl_dw].[p_InsertDimTime]
--	SELECT * FROM Template_DataWarehouse.dim.Time
--	SELECT * FROM internal.Error ORDER BY DateCreated DESC

--==================================================================================================================

BEGIN
    SET XACT_ABORT ON							-- Keyword to enable the XACT Transaction Handling
    SET NOCOUNT ON								-- Eliminate any dataset counts

    DECLARE @returnValue		INT = 0			-- Defaults to returning 0
	,		@JobLogKey			INT				-- Identity for Job Log Table
    ,		@stepNo				INT = 1			-- unique step counter 
    ,		@section			VARCHAR(255)	-- document your steps by setting this variable

	-- Log the start time of the Procedure
    --EXEC [internal].[p_InsertJobLog] @procId = @@PROCID, @JobLogKey = @JobLogKey OUTPUT
	
    BEGIN TRY

		SET @section = 'Insert time records'
		
		--EXEC [internal].[p_InsertJobLog] @section = @section, @stepNo = @stepNo OUTPUT, @JobLogKey = @JobLogKey OUTPUT

		--Load time data for every minute of a day (for seconds, the number table needs to generate 86,400 numbers)
		DECLARE @Time TIME

		SET @Time = '00:00'
		
		; WITH Numbers (N) 
			AS 
			(
				SELECT		ROW_NUMBER() OVER (ORDER BY n1)-1 AS N
				FROM		(VALUES(1),(1),(1),(1),(1),(1),(1),(1),(1),(1))			t1 (n1) --10
				CROSS JOIN	(VALUES(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1)) t2 (n2) --120
				CROSS JOIN	(VALUES(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1)) t3 (n3) --1440
			), t1 as
			(
			SELECT	DATEADD(MINUTE, N, @Time) AS TimeInMinutes
				FROM	Numbers 
				WHERE	N < 1440 -- or 86400 for seconds
			)
			, t2 as
			(
			SELECT
				CAST(LEFT(CONVERT(VARCHAR(5), TimeInMinutes, 108),2) + RIGHT(CONVERT(VARCHAR(5), TimeInMinutes, 108), 2) AS int) AS TimeKey
			,	CAST(LEFT(CONVERT(VARCHAR(5), TimeInMinutes, 108),2) AS INT) AS HourPartInt
			,	LEFT(CONVERT(VARCHAR(5), TimeInMinutes, 108),2) AS HourPartText
			,	CAST(RIGHT(CONVERT(VARCHAR(5), TimeInMinutes, 108), 2) AS INT) AS MinPartInt
			,	CONVERT(VARCHAR(5), TimeInMinutes, 108) AS TwentyFourHourTime
			,	CONVERT(VARCHAR(8), TimeInMinutes, 100) AS TwelveHourTime
			FROM t1
			)
			, 
			 cte_Load as
			(
			SELECT
				t2.TimeKey
			,	t2.TwentyFourHourTime
			,	t2.TwelveHourTime
			,	((HourPartInt * 4) + (MinPartInt / 15)) + 1 as Rounded15MinsKey
			,	RTRIM(HourPartText + ':' + RIGHT(CAST(100 + (( MinPartInt / 15) * 15) AS VARCHAR(3)),2)) AS Rounded15MinsName

			FROM t2
			)

		MERGE 
			dim.Time AS TARGET
		USING 
			cte_Load
		ON
				TARGET.TimeKey		= cte_Load.TimeKey
		WHEN MATCHED THEN
		UPDATE
			SET 
			TwentyFourHourTime	= cte_Load.TwentyFourHourTime
		,	TwelveHourTime		= cte_Load.TwelveHourTime
		,	Rounded15MinsKey	= cte_Load.Rounded15MinsKey
		,	Rounded15MinsName	= cte_Load.Rounded15MinsName

		WHEN NOT MATCHED BY TARGET THEN
		INSERT
			(
				TimeKey
			,	TwentyFourHourTime
			,	TwelveHourTime
			,	Rounded15MinsKey
			,	Rounded15MinsName
			,	RecordCreatedDate
			)
		VALUES
			(
				TimeKey
			,	TwentyFourHourTime
			,	TwelveHourTime
			,	Rounded15MinsKey
			,	Rounded15MinsName
			,	GetDate()
			)
		;

    END TRY

    BEGIN CATCH

		-- Log error the error and raise it again
		EXECUTE [internal].[p_LogAndRaiseSQLError] @section = @section, @JobLogKey = @JobLogKey

		SET @returnValue = 1 -- Set different return value as an exception has been generated

    END CATCH

    -- Log the end of the Procedure Run, success or otherwise    
    EXEC [internal].[p_InsertJobLog] @JobLogKey = @JobLogKey

    RETURN @returnValue

END