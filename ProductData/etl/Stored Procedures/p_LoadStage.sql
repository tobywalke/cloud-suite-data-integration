﻿CREATE PROCEDURE [etl].[p_LoadStage] 
AS
--==================================================================================================================
--Author:		Dave Morrow - ported from KUK Solution
--Created date: 05/06/2018
--Description:	Inserts data from Azure storage to staging
-- ------------------------------------------------------------------------------------------------------------------
--Revision History:
--	Date			Person					Change

-- ------------------------------------------------------------------------------------------------------------------
--Debug Code:



--==================================================================================================================

BEGIN
    SET NOCOUNT ON								-- Eliminate any dataset counts

      DECLARE @returnValue		INT = 0			-- Defaults to returning 0
    ,		@initialJobLogKey	INT				-- Stores the initial job log key to use when an exception occcurs
	,		@jobKey				INT				-- Identity for Job Table
	,		@JobLogKey			INT				-- Identity for Job Log Table
    ,		@stepNo				INT = 1			-- unique step counter 
    ,		@section			NVARCHAR(255)	-- document your steps by setting this variable


    BEGIN TRY

		UPDATE	internal.[Configuration]
		SET		ETLInProgress ='Yes'
		,		TruncateStagingFlag = 'Yes'

		DECLARE @filestatuskey int =  (SELECT FileStatusKey FROM internal.FileStatus WHERE FileStatusName = 'File Ready for Processing')

		-- Log the start time of the Procedure
		EXEC [internal].[p_InsertJobLog] @procId = @@PROCID, @jobKey = @jobKey OUTPUT, @jobLogKey = @jobLogKey OUTPUT;
		SET @initialJobLogKey =  @JobLogKey


		SET @section = 'Set all latest logged files to new job key'
		UPDATE internal.FileLog
		SET JobKey = @jobKey
		WHERE FileStatusKey = @filestatuskey

		SET @section = 'Truncate all staging'
		EXEC [internal].[p_TruncateTablesInSchema] 'stage_csv'
				

		SET @section = 'Select files to be processed'
		EXEC [internal].[p_InsertJobLog] @section = @section, @stepNo = @stepNo OUTPUT, @jobLogKey = @jobLogKey OUTPUT;		

		DECLARE		@RowId					INT = 1
		,			@MaxRowId				INT
		,			@SQL					NVARCHAR(MAX)
		,			@FileLogKey				VARCHAR(20)
		,			@StagingProcedureName	VARCHAR(100)
		,			@PassedStatusKey		INT = (SELECT FileStatusKey FROM internal.FileStatus WHERE FileStatusName = 'File Processed - Passed')
		,			@FailedStatusKey		INT = (SELECT FileStatusKey FROM internal.FileStatus WHERE FileStatusName = 'File Processed - Failed')

		DECLARE		@FilesToProcess			TABLE (RowId INT, FileLogKey INT, StagingProcedureName VARCHAR(100))

		INSERT INTO	@FilesToProcess
		(			RowId
		,			FileLogKey
		,			StagingProcedureName)
		SELECT		ROW_NUMBER() OVER(ORDER BY fl.FileLogKey) AS RowId
		,			fl.FileLogKey
		,			ft.StagingProcedureName
		FROM		internal.FileLog fl
		INNER JOIN	internal.FileType ft
		ON			fl.FileTypeKey = ft.FileTypeKey
		INNER JOIN	internal.FileStatus fs
		ON			fl.FileStatusKey = fs.FileStatusKey
		WHERE		fs.FileStatusName = 'File Ready for Processing'

		--SELECT * FROM @FilesToProcess

		SET @MaxRowId = (SELECT MAX(RowId) FROM @FilesToProcess)

		--SELECT @MaxRowId

		SET @section = 'Load files to staging'
		EXEC [internal].[p_InsertJobLog] @section = @section, @stepNo = @stepNo OUTPUT, @jobLogKey = @jobLogKey OUTPUT;		

		WHILE @RowId <= @MaxRowId
		BEGIN

			BEGIN TRY

				SELECT		@FileLogKey = FileLogKey
				,			@StagingProcedureName = StagingProcedureName
				FROM		@FilesToProcess
				WHERE		RowId = @RowId

				SET @section = 'Loading FileLogKey=' + CAST(@FileLogKey AS NVARCHAR(8)) + ', Proc=' + @StagingProcedureName
				EXEC [internal].[p_InsertJobLog] @section = @section, @stepNo = @stepNo OUTPUT, @jobLogKey = @jobLogKey OUTPUT;		

				SET			@SQL = 'EXEC ' + @StagingProcedureName + ' ' + @FileLogKey
				EXEC		sp_executesql @SQL

				SET			@RowId = @RowId + 1

				UPDATE		internal.FileLog
				SET			FileStatusKey = @PassedStatusKey
				WHERE		FileLogKey = @FileLogKey

				SET @section = 'Successful stage of FileLogKey=' + CAST(@FileLogKey AS NVARCHAR(8)) + ', Proc=' + @StagingProcedureName
				EXEC [internal].[p_InsertJobLog] @section = @section, @stepNo = @stepNo OUTPUT, @jobLogKey = @jobLogKey OUTPUT;		

			END TRY

			BEGIN CATCH

				SET @section = 'Error loading to stage on filelogkey='+ CAST(@FileLogKey AS NVARCHAR(8))
				EXEC [internal].[p_InsertJobLog] @section = @section, @stepNo = @stepNo OUTPUT, @jobLogKey = @jobLogKey OUTPUT;		

				SET			@RowId = @RowId + 1

				UPDATE		internal.FileLog
				SET			FileStatusKey = @FailedStatusKey
				WHERE		FileLogKey = @FileLogKey

			END CATCH

		END

		SET @section = 'Update Job with end of staging'
		EXEC [internal].[p_InsertJobLog] @section = @section, @stepNo = @stepNo OUTPUT, @jobLogKey = @jobLogKey OUTPUT;
			
		EXEC [internal].[p_UpdateJob] 'StageLoadComplete'
			

    END TRY

    BEGIN CATCH

		-- Log the error and raise it again
		EXECUTE [internal].[p_LogAndRaiseSQLError] @section = @section, @JobLogKey = @JobLogKey

		-- Update job with failure
		EXEC [internal].[p_UpdateJob] 'Failure'

		UPDATE	internal.[Configuration]
		SET		ETLInProgress ='No'
		,		TruncateStagingFlag = 'Yes'

		SET @returnValue = 1 -- Set different return value as an exception has been generated

    END CATCH

    -- Log the end of the Procedure Run, success or otherwise    
    EXEC [internal].[p_InsertJobLog] @JobLogKey = @JobLogKey

    RETURN @returnValue

END