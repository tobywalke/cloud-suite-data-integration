﻿CREATE TABLE [dim].[Time] (
    [TimeKey]            INT           NOT NULL,
    [TwentyFourHourTime] CHAR (8)      NOT NULL,
    [TwelveHourTime]     CHAR (11)     NOT NULL,
    [Rounded15MinsKey]   INT           NULL,
    [Rounded15MinsName]  NVARCHAR (20) NULL,
    [RecordCreatedDate]  DATETIME      DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Time] PRIMARY KEY CLUSTERED ([TimeKey] ASC)
);

