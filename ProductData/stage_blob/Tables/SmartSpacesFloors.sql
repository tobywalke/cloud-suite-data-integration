﻿CREATE TABLE [stage_blob].[SmartSpacesFloors]
(
	[_attachments] [nvarchar](50) NULL,
	[name] [nvarchar](50) NULL,
	[_etag] [nvarchar](50) NULL,
	[_rid] [nvarchar](50) NULL,
	[_self] [nvarchar](50) NULL,
	[_ts] [nvarchar](50) NULL,
	[completed] [nvarchar](50) NULL,
	[customerId] [nvarchar](50) NULL,
	[insertdate] [nvarchar](50) NULL,
	[entity] [nvarchar](50) NULL,
	[floorPlan] [nvarchar](100) NULL,
	[floorPlanUrl] [nvarchar](100) NULL,
	[floorid] [nvarchar](100) NULL,
	[picture] [nvarchar](50) NULL,
	[updateMask] [varchar](max) NULL,
	[filelogkey] INT
)
