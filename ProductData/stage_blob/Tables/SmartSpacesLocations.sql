﻿
CREATE TABLE [stage_blob].[SpartSpacesLocations](
	[Locationid] [nvarchar](50) NULL,
	[date] [nvarchar](50) NULL,
	[completed] [nvarchar](50) NULL,
	[entity] [nvarchar](50) NULL,
	[customerId] [nvarchar](50) NULL,
	[addressLine1] [nvarchar](50) NULL,
	[addressLine2] [nvarchar](50) NULL,
	[postCode] [nvarchar](50) NULL,
	[city] [nvarchar](50) NULL,
	[longitude] [nvarchar](50) NULL,
	[latitude] [nvarchar](50) NULL
) 
