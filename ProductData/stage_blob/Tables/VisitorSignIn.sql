﻿
CREATE TABLE [stage_blob].[VisitorSignIn](
	[id] [nvarchar](50) NULL,
	[nameofvisitor] [nvarchar](50) NULL,
	[carRegistration] [nvarchar](50) NULL,
	[company] [nvarchar](50) NULL,
	[signInTime] [nvarchar](50) NULL,
	[signOutTime] [nvarchar](50) NULL,
	[isRFID] [nvarchar](50) NULL,
	[isEmployeeSignIn] [nvarchar](50) NULL,
	[floorId] [nvarchar](50) NULL,
	[visitorType] [nvarchar](50) NULL,
	[entity] [nvarchar](50) NULL,
	[customerId] [nvarchar](50) NULL,
	[locationId] [nvarchar](50) NULL,
	[visitinguserid] [nvarchar](50) NULL,
	[visitingname] [nvarchar](50) NULL,
	[visitingemail] [nvarchar](50) NULL
)

