﻿

CREATE TABLE [stage_blob].[SpartSpacesRooms](
	[id] [nvarchar](50) NULL,
	[condecoId] [nvarchar](50) NULL,
	[outlookId] [nvarchar](50) NULL,
	[meetingroomname] [nvarchar](50) NULL,
	[description] [nvarchar](50) NULL,
	[floorId] [nvarchar](50) NULL,
	[notes] [nvarchar](50) NULL,
	[entity] [nvarchar](50) NULL,
	[customerId] [nvarchar](50) NULL,
	[locationId] [nvarchar](50) NULL,
	[capacity] [nvarchar](50) NULL,
	[wifi] [nvarchar](50) NULL,
	[interactiveWhiteboard] [nvarchar](50) NULL,
	[display] [nvarchar](50) NULL,
	[deskPower] [nvarchar](50) NULL,
	[conferenceCall] [nvarchar](50) NULL,
	[serviceAvailable] [nvarchar](50) NULL,
	[accessible] [nvarchar](50) NULL
) 

