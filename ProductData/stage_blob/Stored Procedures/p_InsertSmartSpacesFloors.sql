﻿CREATE PROCEDURE [stage_blob].[p_InsertSmartSpacesFloors]
(
	@FileLogKey	INT
,	@debug INT = 1
)
AS
--==================================================================================================================
--Author:		Ridgian
--Created date: 10/11/2015
--Description:	Shreds the blob files for the smart spaces floor data
-- ------------------------------------------------------------------------------------------------------------------
--Revision History:
--	Date			Person					Change
-- ------------------------------------------------------------------------------------------------------------------
--Debug Code:


--==================================================================================================================

BEGIN
    SET XACT_ABORT ON							-- Keyword to enable the XACT Transaction Handling
    SET NOCOUNT ON								-- Eliminate any dataset counts

    DECLARE @returnValue		INT = 0			-- Defaults to returning 0
	,		@JobLogKey			INT				-- Identity for Job Log Table
    ,		@stepNo				INT = 1			-- unique step counter 
    ,		@section			VARCHAR(255)	-- document your steps by setting this variable

	-- Log the start time of the Procedure
    EXEC [internal].[p_InsertJobLog] @procId = @@PROCID, @JobLogKey = @JobLogKey OUTPUT
	
    BEGIN TRY

		SET @section = 'Insert time records'
		
		EXEC [internal].[p_InsertJobLog] @section = @section, @stepNo = @stepNo OUTPUT, @JobLogKey = @JobLogKey OUTPUT

		--Load time data for every minute of a day (for seconds, the number table needs to generate 86,400 numbers)
		DECLARE @FilePath VARCHAR(255)
		,		@StorageAccount VARCHAR(100) = (SELECT AzureFileStorage FROM internal.Configuration)
		,		@Velocity  VARCHAR(50)
		,		@JSONSQL NVARCHAR(MAX)
		,		@JSONRaw NVARCHAR(MAX)

		SELECT	@FilePath = fl.FilePath
		,		@Velocity= dt.DataTypeVelocity
		FROM	internal.FileLog fl
		INNER JOIN
				internal.FileType ft
		ON		fl.FIleTypeKey = ft.FileTypeKey
		INNER JOIN
				internal.DataType dt
		ON		ft.DataTypeKey = dt.DataTypeKey
		WHERE	fl.FileLogKey = @FileLogKey


		DROP TABLE IF EXISTS #data

		CREATE TABLE #data
		(
			JSONString VARCHAR(MAX)
		)

		SET @JSONSQL =
		'
		INSERT INTO #data
		SELECT Bulkcolumn FROM OPENROWSET
		(		BULK '''+@FilePath+'''
		,		DATA_SOURCE = '''+@StorageAccount+'''
		,		SINGLE_CLOB
		)		AS	 DATA
		'
		IF @debug <> 1
		BEGIN
			PRINT @JSONSQL
		END ELSE BEGIN
			EXECUTE sp_executesql @JSONSQL
		END

		SET @section = 'Fix JSON string'
		SET @JSONRAW = (SELECT Jsonstring FROM #data)
		--SET @JSONRAW = REPLACE(@JSONRAW,'{',',{') 
		--SET @JSONRAW = SUBSTRING(@JSONRAW,2,LEN(@JSONRAW)-1)
		--SET @JSONRAW = REPLACE(@JSONRAW,':,{',':{')
		SET @JSONRAW = '[' + @JSONRaw + ']'

		IF @Velocity = 'Steam'
			BEGIN
				-- Insert straight into mart 
				SELECT 1
			END
		ELSE
			BEGIN

				INSERT INTO [stage_blob].[SmartSpacesFloors]
						   ([_attachments]
						   ,[name]
						   ,[_etag]
						   ,[_rid]
						   ,[_self]
						   ,[_ts]
						   ,[completed]
						   ,[customerId]
						   ,[insertdate]
						   ,[entity]
						   ,[floorPlan]
						   ,[floorPlanUrl]
						   ,[floorid]
						   ,[picture]
						   ,[updateMask]
						   ,[filelogkey]
						  )
				SELECT		_attachments
				,			[name]	
				,			_etag		
				,			_rid		
				,			_self		
				,			_ts		
				,			completed	
				,			customerId	
				,			insertdate	
				,			entity		
				,			floorPlan	
				,			floorPlanUrl
				,			floorid			
				,			picture	
				,			updateMask		
				,			@FileLogKey
				FROM OPENJSON(@JSONRaw)
				CROSS APPLY	OPENJSON(Value) 
				WITH (
						_attachments	nvarchar(50)
						, [name]		nvarchar(50)
						,_etag			nvarchar(50)
						,_rid			nvarchar(50)
						,_self			nvarchar(50)
						,_ts			nvarchar(50)
						,completed		nvarchar(50)
						,customerId		nvarchar(50)
						,insertdate		nvarchar(50) '$.date'
						,entity			nvarchar(50)
						,floorPlan		nvarchar(100)
						,floorPlanUrl	nvarchar(100)
						,floorid		nvarchar(100)'$.id'
						,picture		nvarchar(50)
						,updateMask		varchar(MAX)
					)

			END

    END TRY

    BEGIN CATCH

		-- Log error the error and raise it again
		EXECUTE [internal].[p_LogAndRaiseSQLError] @section = @section, @JobLogKey = @JobLogKey

		SET @returnValue = 1 -- Set different return value as an exception has been generated

    END CATCH

    -- Log the end of the Procedure Run, success or otherwise    
    EXEC [internal].[p_InsertJobLog] @JobLogKey = @JobLogKey

    RETURN @returnValue

END