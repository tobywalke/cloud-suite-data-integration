﻿CREATE PROCEDURE [stage_blob].[p_InsertVisitorSignIn]
(
	@FileLogKey	INT
,	@debug INT = 1
)
AS
--==================================================================================================================
--Author:		Ridgian
--Created date: 10/11/2015
--Description:	Shreds the blob files for the svisitor sign in data
-- ------------------------------------------------------------------------------------------------------------------
--Revision History:
--	Date			Person					Change
-- ------------------------------------------------------------------------------------------------------------------
--Debug Code:


--==================================================================================================================

BEGIN
    SET XACT_ABORT ON							-- Keyword to enable the XACT Transaction Handling
    SET NOCOUNT ON								-- Eliminate any dataset counts

    DECLARE @returnValue		INT = 0			-- Defaults to returning 0
	,		@JobLogKey			INT				-- Identity for Job Log Table
    ,		@stepNo				INT = 1			-- unique step counter 
    ,		@section			VARCHAR(255)	-- document your steps by setting this variable

	-- Log the start time of the Procedure
    EXEC [internal].[p_InsertJobLog] @procId = @@PROCID, @JobLogKey = @JobLogKey OUTPUT
	
    BEGIN TRY

		SET @section = 'Insert time records'
		
		EXEC [internal].[p_InsertJobLog] @section = @section, @stepNo = @stepNo OUTPUT, @JobLogKey = @JobLogKey OUTPUT

		
		DECLARE @FilePath VARCHAR(255)
		,		@StorageAccount VARCHAR(100) = (SELECT AzureFileStorage FROM internal.Configuration)
		,		@Velocity  VARCHAR(50)
		,		@JSONSQL NVARCHAR(MAX)
		,		@JSONRaw NVARCHAR(MAX)

		SELECT	@FilePath = fl.FilePath
		,		@Velocity= dt.DataTypeVelocity
		FROM	internal.FileLog fl
		INNER JOIN
				internal.FileType ft
		ON		fl.FIleTypeKey = ft.FileTypeKey
		INNER JOIN
				internal.DataType dt
		ON		ft.DataTypeKey = dt.DataTypeKey
		WHERE	fl.FileLogKey = @FileLogKey


		DROP TABLE IF EXISTS #data

		CREATE TABLE #data
		(
			JSONString VARCHAR(MAX)
		)

		SET @JSONSQL =
		'
		INSERT INTO #data
		SELECT Bulkcolumn FROM OPENROWSET
		(		BULK '''+@FilePath+'''
		,		DATA_SOURCE = '''+@StorageAccount+'''
		,		SINGLE_CLOB
		)		AS	 DATA
		'
		IF @debug <> 1
		BEGIN
			PRINT @JSONSQL
		END ELSE BEGIN
			EXECUTE sp_executesql @JSONSQL
		END

		SET @section = 'Fix JSON string'
		SET @JSONRAW = (SELECT Jsonstring FROM #data)
		--SET @JSONRAW = REPLACE(@JSONRAW,'{',',{') 
		--SET @JSONRAW = SUBSTRING(@JSONRAW,2,LEN(@JSONRAW)-1)
		--SET @JSONRAW = REPLACE(@JSONRAW,':,{',':{')
		SET @JSONRAW = '[' + @JSONRaw + ']'

		IF @Velocity = 'Steam'
			BEGIN
				-- Insert straight into mart 
				SELECT 1
			END
		ELSE
			BEGIN
			INSERT INTO [stage_blob].[VisitorSignIn]
			(
				[id]
			   ,[nameofvisitor]
			   ,[carRegistration]
			   ,[company]
			   ,[signInTime]
			   ,[signOutTime]
			   ,[isRFID]
			   ,[isEmployeeSignIn]
			   ,[floorId]
			   ,[visitorType]
			   ,[entity]
			   ,[customerId]
			   ,[locationId]
			   ,[visitinguserid]
			   ,[visitingname]
			   ,[visitingemail]
		   )


				SELECT	
					id				
				,	[nameofvisitor]	
				,	carRegistration	
				,	company			
				,	signInTime		
				,	signOutTime		
				,	isRFID			
				,	isEmployeeSignIn
				,	floorId			
				,	visitorType		
				,	entity			
				,	customerId		
				,	locationId			
				,	[visitinguserid]
				,	[visitingname]	
				,	[visitingemail]	

				FROM OPENJSON(@JSONRaw)
				CROSS APPLY	OPENJSON(Value) 
				WITH (
							id						nvarchar(50)
						,	[nameofvisitor]			nvarchar(50) '$.name'
						,	carRegistration			nvarchar(50)
						,	company					nvarchar(50)
						,	signInTime				nvarchar(50)
						,	signOutTime				nvarchar(50)
						,	isRFID					nvarchar(50)
						,	isEmployeeSignIn		nvarchar(50)
						,	floorId					nvarchar(50)
						,	visitorType				nvarchar(50)
						,	entity					nvarchar(50)
						,	customerId				nvarchar(50)
						,	locationId				nvarchar(50)
						,	floorId					nvarchar(50)
						,	visiting			nvarchar(max) as JSON
					)
					CROSS APPLY	OPENJSON(visiting) 
					WITH			(
										[visitinguserid]	nvarchar(50) '$.id'
									,	[visitingname]		nvarchar(50) '$.name'
									,	[visitingemail]		nvarchar(50) '$.email'
									)

			END

    END TRY

    BEGIN CATCH

		-- Log error the error and raise it again
		EXECUTE [internal].[p_LogAndRaiseSQLError] @section = @section, @JobLogKey = @JobLogKey

		SET @returnValue = 1 -- Set different return value as an exception has been generated

    END CATCH

    -- Log the end of the Procedure Run, success or otherwise    
    EXEC [internal].[p_InsertJobLog] @JobLogKey = @JobLogKey

    RETURN @returnValue

END