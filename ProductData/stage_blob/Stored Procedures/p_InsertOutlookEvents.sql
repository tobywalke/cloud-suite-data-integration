﻿CREATE PROCEDURE [stage_blob].[p_InsertOutlookEvents]
(
	@FileLogKey	INT
,	@debug INT = 1
)
AS
--==================================================================================================================
--Author:		Ridgian
--Created date: 10/11/2015
--Description:	Shreds the blob files for the smart spaces floor data
-- ------------------------------------------------------------------------------------------------------------------
--Revision History:
--	Date			Person					Change
-- ------------------------------------------------------------------------------------------------------------------
--Debug Code:


--==================================================================================================================

BEGIN
    SET XACT_ABORT ON							-- Keyword to enable the XACT Transaction Handling
    SET NOCOUNT ON								-- Eliminate any dataset counts

    DECLARE @returnValue		INT = 0			-- Defaults to returning 0
	,		@JobLogKey			INT				-- Identity for Job Log Table
    ,		@stepNo				INT = 1			-- unique step counter 
    ,		@section			VARCHAR(255)	-- document your steps by setting this variable

	-- Log the start time of the Procedure
    EXEC [internal].[p_InsertJobLog] @procId = @@PROCID, @JobLogKey = @JobLogKey OUTPUT
	
    BEGIN TRY

		SET @section = 'Insert time records'
		
		EXEC [internal].[p_InsertJobLog] @section = @section, @stepNo = @stepNo OUTPUT, @JobLogKey = @JobLogKey OUTPUT

		
		DECLARE @FilePath VARCHAR(255)
		,		@StorageAccount VARCHAR(100) = (SELECT AzureFileStorage FROM internal.Configuration)
		,		@Velocity  VARCHAR(50)
		,		@JSONSQL NVARCHAR(MAX)
		,		@JSONRaw NVARCHAR(MAX)

		SELECT	@FilePath = fl.FilePath
		,		@Velocity= dt.DataTypeVelocity
		FROM	internal.FileLog fl
		INNER JOIN
				internal.FileType ft
		ON		fl.FIleTypeKey = ft.FileTypeKey
		INNER JOIN
				internal.DataType dt
		ON		ft.DataTypeKey = dt.DataTypeKey
		WHERE	fl.FileLogKey = @FileLogKey


		DROP TABLE IF EXISTS #data

		CREATE TABLE #data
		(
			JSONString VARCHAR(MAX)
		)

		SET @JSONSQL =
		'
		INSERT INTO #data
		SELECT Bulkcolumn FROM OPENROWSET
		(		BULK '''+@FilePath+'''
		,		DATA_SOURCE = '''+@StorageAccount+'''
		,		SINGLE_CLOB
		)		AS	 DATA
		'
		IF @debug <> 1
		BEGIN
			PRINT @JSONSQL
		END ELSE BEGIN
			EXECUTE sp_executesql @JSONSQL
		END

		SET @section = 'Fix JSON string'
		SET @JSONRAW = (SELECT Jsonstring FROM #data)
		--SET @JSONRAW = REPLACE(@JSONRAW,'{',',{') 
		--SET @JSONRAW = SUBSTRING(@JSONRAW,2,LEN(@JSONRAW)-1)
		--SET @JSONRAW = REPLACE(@JSONRAW,':,{',':{')
		SET @JSONRAW = '[' + @JSONRaw + ']'

		IF @Velocity = 'Steam'
			BEGIN
				-- Insert straight into mart 
				SELECT 1
			END
		ELSE
			BEGIN

				INSERT INTO [stage_blob].[OutlookEvents]
						   ([eventsubject]	
				,		startTime		
				,		endTime			
				,		importance		
				,		isAllDay		
				,		[location]		
				,		[locationText]	
				,		isCancelled		
				,		isPrivate		
				,		iCalUId			
				,		sourceType		
				,		entity			
				,		customerId		
				,		spaceId			
				,		locationId		
				,		[emalladdress]
				,		[attendeename]
				,		isHost			
				,		[attendeestatus]
				,		response		
				,		[organiseremail]
				,		[organisername] 
				,		[locationdisplayName]
				,		[locationType]			
				,		[locationuniqueId]		
				,		[locationuniqueIdType]
						  )
				SELECT	[eventsubject]	
				,		startTime		
				,		endTime			
				,		importance		
				,		isAllDay		
				,		[location]		
				,		[locationText]	
				,		isCancelled		
				,		isPrivate		
				,		iCalUId			
				,		sourceType		
				,		entity			
				,		customerId		
				,		spaceId			
				,		locationId		
				,		[emalladdress]
				,		[attendeename]
				,		isHost			
				,		[attendeestatus]
				,		response		
				,		[organiseremail]
				,		[organisername] 
				,		[locationdisplayName]
				,		[locationType]			
				,		[locationuniqueId]		
				,		[locationuniqueIdType]
				FROM OPENJSON(@JSONRaw)
				CROSS APPLY	OPENJSON(Value) 
				WITH (
							[eventsubject]	nvarchar(50) '$.subject'
						,	startTime		nvarchar(50)
						,	endTime			nvarchar(50)
						,	importance		nvarchar(50)
						,	isAllDay		nvarchar(50)
						,	[location]		nvarchar(50)
						,	[locationText]	nvarchar(50)
						,	isCancelled		nvarchar(50)
						,	isPrivate		nvarchar(50)
						,	iCalUId			nvarchar(50)
						,	sourceType		nvarchar(50)
						,	entity			nvarchar(50)
						,	customerId		nvarchar(50)
						,	spaceId			nvarchar(50)
						,	locationId		nvarchar(50)
						,	locations		nvarchar(max) as JSON
						,	organiser		nvarchar(max) as JSON
						,	attendees		nvarchar(max) as JSON

					)
					CROSS APPLY	OPENJSON(attendees) 
					WITH			(
										[emalladdress]	nvarchar(50) '$.email'
									,	[attendeename]	nvarchar(250) '$.name'
									,	isHost			nvarchar(50)
									,	[attendeestatus] nvarchar(50) '$.status'
									,	response			nvarchar(50)
									)
					CROSS APPLY	OPENJSON(organiser) 
					WITH			(
										[organiseremail] nvarchar(50) '$.email'
									,	[organisername] nvarchar(50) '$.name'
									)
					CROSS APPLY	OPENJSON(locations) 
					WITH			(
										[locationdisplayName]	nvarchar(50) '$.displayName'
									,	[locationType]			nvarchar(50) '$.locationType'
									,	[locationuniqueId]		nvarchar(50) '$.uniqueId'
									,	[locationuniqueIdType]	nvarchar(50) '$.uniqueIdType'
									)

			END

    END TRY

    BEGIN CATCH

		-- Log error the error and raise it again
		EXECUTE [internal].[p_LogAndRaiseSQLError] @section = @section, @JobLogKey = @JobLogKey

		SET @returnValue = 1 -- Set different return value as an exception has been generated

    END CATCH

    -- Log the end of the Procedure Run, success or otherwise    
    EXEC [internal].[p_InsertJobLog] @JobLogKey = @JobLogKey

    RETURN @returnValue

END